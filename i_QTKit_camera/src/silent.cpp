//
//  Created by Arnaud Blanchard on 06/11/14.
//  Copyright ETIS 2014. All rights reserved.
//
#include "silent.hpp"
#include "main.hpp"
#include "blQTKit.h"

#include "blc.h"
#include <sys/signal.h>
#include <stdlib.h>

/**
 @main
 Exemple of using the console to display the camera
 */

static int initialised = 0;
static int width, height = 0;
static uint32_t input_format;

static void command_callback(char const*, char const *argument, void *user_data)
{
    
    uint32_t input_format_string, output_format_string;
    (void)argument;
    
    switch (*(char*)user_data)
    {
        case   0:if (status==RUN)
        {
            status=PAUSE;
            fprintf(stderr, "%d iterations, average period: %.3lf ms\n", iteration, period/1000.);
            iteration=0;
            blc_command_display_help();
            blc_command_interpret();
        } else status=RUN;
            break;
        case 'i':
            input_format_string=htonl(input_format);
            output_format_string=htonl(output_format);
            fprintf(stderr, "input:%.4s output:%.4s\n", (char*)&input_format_string, (char*)&output_format_string);
            printf("i");
            fflush(stdout);break;
        case 'q':printf("q"); status=STOP;break;
    }
}

static void quit_silent_mode()
{
    fflush(stdout);
    if (channel) channel->unlink();
    fprintf(stderr, "\nQuitting %s.\n", blc_program_name);
}

void silent_mode_callback(CVImageBufferRef videoFrame, void *user_data)
{
    blc_mem mem;
    int bytes_per_pixel, retval;
    fd_set rfds, rdfs_error;
    struct timeval time, no_time={10,0};
    (void)user_data;
    
    
    if (!initialised)
    {
        input_format = ntohl(CVPixelBufferGetPixelFormatType(videoFrame));
        if (output_format==0) output_format = input_format;
        
        width= CVPixelBufferGetWidth(videoFrame);
        height=  CVPixelBufferGetHeight(videoFrame);
        bytes_per_pixel=blc_get_bytes_per_pixel(output_format);
        input_image.size=blc_get_bytes_per_pixel(input_format)*width*height;
        if (bytes_per_pixel==1) channel = create_or_open_blc_channel(channel_name, BLC_SEM_PROTECT, 'UIN8', output_format, NULL, 2, width, height);
        else  channel = create_or_open_blc_channel(channel_name, BLC_SEM_PROTECT, 'UIN8', output_format, NULL, 3, bytes_per_pixel, width, height);
        if (synchronous_mode) printf("%s\n", channel->name);
        fflush(stdout);
        initialised = 1;
    }
    channel->protect();
    CVPixelBufferLockBaseAddress(videoFrame, 0);
    input_image.data = (char*)CVPixelBufferGetBaseAddress(videoFrame);
    CLEAR(time);
    us_time_diff(&time);
    copy_image(&input_image, input_format, channel, output_format);
    CVPixelBufferUnlockBaseAddress(videoFrame, 0);
    channel->protect(0);
    
    blc_command_try_to_interpret();
    //    if (synchronous_mode)  printf(".");
    
   

//if (retval){
        if (synchronous_mode)
        {
            
            while (write(STDOUT_FILENO, ".", 1)==-1)
            {
                fprintf(stderr, "No one listening -> pause\n");
                sleep(1);
        //        fprintf(stderr, "resume\n");
            }
            
        
      /*  if (fflush(stdout)==EOF){
            if (errno==EPIPE) {
                do
                {
                    fprintf(stderr, "No one listening -> pause\n");
                    fprintf(stderr, "resume\n");
                    printf("%s\n", channel->name);
                }
                while(feof(stdout));
            }
        }*/
        }
    
    if (status == STOP) exit(0);
}

void start_silent_mode()
{

    blc_command_add("i", command_callback, NULL, "info", (void*)"i");
    blc_command_add("\0", command_callback, NULL, "pause", (void*)"\0");
    blc_command_add("q", command_callback, NULL, "quit", (void*)"q");
    atexit(quit_silent_mode);
    init_capture(silent_mode_callback, NULL);
}


