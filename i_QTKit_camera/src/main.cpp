//
//  Created by Arnaud Blanchard on 06/11/14.
//  Copyright ETIS 2014. All rights reserved.
//

#include "main.hpp"
#include "blQTKit.h"
#include "interactive.hpp"
#include "silent.hpp"
#include "blc.h"
#include <sys/signal.h>
#include <stdlib.h>
#include <unistd.h>

/**
 @main
 Exemple of using the console to display the camera
 */

char const *output_pipe_name;
int status=RUN;
double period;
int iteration=1;
struct timeval timestamp={};
char const *force_ansi=NULL;
blc_mem input_image;
int scale;
uint32_t output_format;
char  const *channel_name;
blc_channel *channel=NULL;
int synchronous_mode=1;
int initialised=0;
int width, height = 0;
uint32_t input_format;
int pipe_out;
char const *output_format_option;

static void command_callback(char const*command, char const *, void *)
{
    
    uint32_t input_format_string, output_format_string;
    
    switch (command[0])
    {
        case   0:if (status==RUN)
        {
            status=PAUSE;
            fprintf(stderr, "%d iterations, average period: %.3lf ms\n", iteration, period/1000.);
            iteration=0;
            blc_command_display_help();
            blc_command_interpret();
        } else status=RUN;
            break;
        case 'd':
            printf("d");
            fflush(stdout);
            break;
        case 'i':
            fprintf(stderr, "%s: input:%.4s output:%.4s\n\n", blc_program_name, UINT32_TO_STRING(input_format_string, input_format), UINT32_TO_STRING(output_format_string, output_format));
            fflush(stderr);
            printf("i");
            fflush(stdout);
            break;
        case 'q':printf("q"); status=STOP;break;
    }
}


static void quit_silent_mode()
{
    fflush(stdout);
    if (channel) channel->unlink();
    fprintf(stderr, "\nQuitting %s.\n", blc_program_name);
}

void init_pipe()
{
    if (output_pipe_name){
        fprintf(stderr, "Waiting connection on pipe '%s'\n", output_pipe_name);
        freopen(output_pipe_name, "w", stdout);
    }
    if (isatty(STDOUT_FILENO)) pipe_out=0;
    else {
        pipe_out=1;
        printf("%s\n", channel->name);
        fflush(stdout);
    }
}

void capture_callback(CVImageBufferRef videoFrame, void *)
{
    blc_mem mem;
    int bytes_per_pixel;
    struct timeval time;
    
    if (!initialised)
    {
        input_format = ntohl(CVPixelBufferGetPixelFormatType(videoFrame));
        if (output_format_option != NULL) output_format = STRING_TO_UINT32(output_format_option);
        else output_format = input_format;
        
        width= CVPixelBufferGetWidth(videoFrame);
        height=  CVPixelBufferGetHeight(videoFrame);
        bytes_per_pixel=blc_get_bytes_per_pixel(output_format);
        input_image.size=blc_get_bytes_per_pixel(input_format)*width*height;
        if (bytes_per_pixel==1) channel = create_or_open_blc_channel(channel_name, BLC_SEM_PROTECT, 'UIN8', output_format, NULL, 2, width, height);
        else  channel = create_or_open_blc_channel(channel_name, BLC_SEM_PROTECT, 'UIN8', output_format, NULL, 3, bytes_per_pixel, width, height);
        init_pipe();
        fprintf(stderr, "start\n");
        initialised = 1;
    }
    
    channel->protect();
    CVPixelBufferLockBaseAddress(videoFrame, 0);
    input_image.data = (char*)CVPixelBufferGetBaseAddress(videoFrame);
    CLEAR(time);
    us_time_diff(&time);
    copy_image(&input_image, input_format, channel, output_format);
    CVPixelBufferUnlockBaseAddress(videoFrame, 0);
    channel->protect(0);
    
    blc_command_try_to_interpret();
    
    if (pipe_out)
    {
        while (write(STDOUT_FILENO, ".", 1)==-1)
        {
            fprintf(stderr, "No one listening -> pause\n");
            if (output_pipe_name) init_pipe();
            else sleep(1);
            fprintf(stderr, "resume\n");

        }
    }
    
    if (status == STOP) exit(0);
}

int main(int argc, char **argv)
{
    char const *help;
    
    program_option_add(&output_format_option, 'f', "format", "Y800", "set ouput video format", NULL);
    program_option_add(&help, 'h', "help", 0, "display help", NULL);
    program_option_add(&channel_name, 'o', "output", "blc_channel", "output channel name", NULL);
    program_option_add(&output_pipe_name, 'O', "output-pipe", "pipe", "output pipe name", NULL);

    program_option_interpret_and_print_title(&argc, &argv);
    
    if (help) program_option_display_help();
    else{
        signal(SIGPIPE, SIG_IGN);

        if (channel_name==NULL) asprintf((char**)&channel_name, "/image%d", getpid());
        
        blc_command_add("", command_callback, NULL, "pause", NULL);
        blc_command_add("d", command_callback, NULL, "switch debug", NULL);
        blc_command_add("h", (type_blc_command_cb)blc_command_display_help, NULL, "help", NULL);
        blc_command_add("i", command_callback, NULL, "info", NULL);
        blc_command_add("q", command_callback, NULL, "quit", NULL);
        atexit(quit_silent_mode);
        init_capture(capture_callback, NULL);    }
    
    return 0;
}
