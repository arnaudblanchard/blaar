//
//  Created by Arnaud Blanchard on 06/11/14.
//  Copyright ETIS 2014. All rights reserved.
//

#include <AudioToolbox/AudioToolbox.h>
#include "blc.h"
#include <sys/signal.h>
#include <stdlib.h>


/**
 @main
 Exemple of using the console to display the camera
 */
enum status {STOP, PAUSE, RUN};
blc_channel *channel=NULL;
AudioQueueRef               queue;
int columns_nb, rows_nb;
size_t buffer_size=1024;

blc_mem graph_buffer(256);

static void quit()
{
    fflush(stdout);
    if (channel) channel->unlink();
    printf("\nQuitting coreaudio.\n");
}

void resize_callback(int new_columns_nb, int new_rows_nb)
{
    columns_nb = new_columns_nb;
    rows_nb = new_rows_nb;
}

void input_queue_callback( void *inUserData, AudioQueueRef inAQ, AudioQueueBufferRef inBuffer, const AudioTimeStamp *inStartTime, UInt32 inNumberPacketDescriptions, const AudioStreamPacketDescription *inPacketDescs )
{
    char const *title="micro";
    size_t i;
    
    if (channel)
    {
        memcpy(channel->data, inBuffer->mAudioData,  channel->lengths[0]);
    }
    else
    {
        graph_buffer.size=columns_nb/3-1;
        FOR(i, graph_buffer.size) graph_buffer.data[i]=128+((char*)inBuffer->mAudioData)[i];
        fprint_graphs(stdout, &graph_buffer, &title, 1,  rows_nb, 256, 128, "time", "intensity ");
    }
    AudioQueueEnqueueBuffer(inAQ, inBuffer, 0, NULL);
}

void command_callback(char const *argument, void *user_data)
{
    (void)argument;
    print_escape_command("0m");
    switch (*(char*)user_data)
    {
            
    }
}

int main(int argc, char **argv)
{
    AudioQueueBufferRef buffer[2];
    AudioStreamBasicDescription audio_format;
    char const *help=NULL, *output=NULL;
    
    int i;
    OSStatus err;
    
    program_option_add(&help, 'h', "help", NULL, "display help");
    program_option_add(&output, 'o', "output", "blc_channel", "name of the channel where the buffer will be put");
    program_set_description("Acquire the audio buffer.");
    program_option_interpret(&argc, &argv);
    
    if (help) program_option_display_help();
    else
    {
        CLEAR(audio_format);
        audio_format.mSampleRate = 44100;
        audio_format.mFormatID= kAudioFormatLinearPCM;
        audio_format.mFormatFlags=kLinearPCMFormatFlagIsSignedInteger | kLinearPCMFormatFlagIsPacked;
        audio_format.mChannelsPerFrame=1;
        audio_format.mBitsPerChannel=8;
        audio_format.mBytesPerPacket = audio_format.mBytesPerFrame = 1;
        audio_format.mFramesPerPacket = 1;
        audio_format.mReserved = 0;
        
        if (output) channel=new blc_channel(output, "p", 'INT8', 'LPCM', NULL, 1, buffer_size);
        else {
            terminal_ansi_detect();
            if (ansi_terminal) terminal_get_size(&columns_nb, &rows_nb);
            terminal_set_resize_callback(resize_callback);
        }

        err=AudioQueueNewInput(&audio_format, input_queue_callback,  NULL,  NULL, NULL, 0, &queue);
        if (err) {
            if (err==kAudio_ParamError) EXIT_ON_ERROR("Parameter error");
            else EXIT_ON_ERROR("Fail to create input audioqueue. Code %d .", err);
        }
        
        FOR(i, 2)
        {
            err = AudioQueueAllocateBuffer(queue, buffer_size, &buffer[i]);
            if (err) EXIT_ON_ERROR("Fail to create input audioqueue. Code %d .", err);
            err = AudioQueueEnqueueBuffer(queue, buffer[i], 0, NULL);
            if (err) EXIT_ON_ERROR("AudioQueueEnqueueBuffer Code %d .", err);
        }
        
        err=AudioQueueStart(queue, NULL);
        if (err) EXIT_ON_ERROR("Fail to create input audioqueuestart. Code %d .", err);
        printf("'q' to quit\n");
        while('q'!=getchar());
    }
    
    return 0;
}
