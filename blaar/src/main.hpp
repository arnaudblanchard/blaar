//
//  main.hpp
//  blc_channels_gtk
//
//  Created by Arnaud Blanchard on 16/10/2015.
//
//

#ifndef MAIN_HPP
#define MAIN_HPP

#include <gtk/gtk.h>
#include "blc.h"
extern dictionary *ini_dictionary;
extern char const *binary_dirname;
extern GtkWidget *main_window;
extern GtkWidget *status_bar;
extern char const *start_filter_name;
extern char const *blaa_dir;

#endif
