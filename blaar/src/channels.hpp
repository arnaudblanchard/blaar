

#ifndef CHANNELS_HPP
#define CHANNELS_HPP
#include <gtk/gtk.h>


int reload_channels(void *user_data);
GtkWidget *create_channels_display();


#endif