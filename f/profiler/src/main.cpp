//
//  Created by Arnaud Blanchard on 06/11/14.
//  Copyright ETIS 2014. All rights reserved.
//

#include "blc.h"

int main(int argc, char **argv)
{
    char const *samples_nb_option="0";
    char const *sample_time_max_option="0";
    char const *help_option=NULL;
    char *end;
    long sample_time_max;
    
    int iterations_nb=0, samples_nb;
    struct timeval timestamp={};
    
    int ch=0;
    long period=0;
    
    program_option_add(&help_option, 'h', "help", 0, "display this help.");
    program_option_add(&samples_nb_option, 'n', "samples-number", 1, "Number of items to make average.");
    program_option_add(&sample_time_max_option, 't', "samples-max-time", 1, "Maximal time in ms to refresh the average.");
    program_option_interpret(&argc, &argv);
    
    if (help_option)program_option_display_help();
    else
    {
        samples_nb = strtol(samples_nb_option, NULL, 10);
        sample_time_max = strtod(sample_time_max_option, &end)*1000;
        if (end == sample_time_max_option) EXIT_ON_ERROR("samples-max-time '%s' is not a number.", sample_time_max_option);
        
        if (samples_nb==0 && sample_time_max==0) samples_nb=1;
        
        while(ch!='q')
        {
            ch = getc(stdin);
            switch (ch)
            {
                case '/':scanf("%*[^\n]"); //This indicates a channel, we do not care
                    break;
                case EOF:fprintf(stderr, "No more input\n");exit(0);break; //No more input.
                default:iterations_nb++;
                    period += diff_us_time(&timestamp);
                    if (((samples_nb != 0) && iterations_nb==samples_nb ) || ((sample_time_max != 0) && period > sample_time_max))
                    {
                        fprintf(stderr, "samples: %d, period: %lfms\n", iterations_nb, (double)period/(iterations_nb*1000));
                        iterations_nb=0;
                        period=0;
                    }
                    break;
            }
        }
    }
    return 0;
}
