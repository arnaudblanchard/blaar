//
//  Created by Arnaud Blanchard on 06/11/14.
//  Copyright ETIS 2014. All rights reserved.
//

#include "blc.h"

enum status {STOP, PAUSE, RUN};

blc_channel *input_channel, *output_channel;
char data_name[NAME_MAX];
int iteration, component_offset=1, is_zoom=0;
double period;
int columns_nb, rows_nb, status=RUN;

static void quit()
{
    fflush(stdout);
    printf("\nQuitting convert_image.");
}

int main(int argc, char **argv)
{
    char channel_name[NAME_MAX];
    int width, height;
    char *data_format;
    char const *input = NULL;
    uint32_t input_pixel_format, output_pixel_format=0;
    char const *output_format=NULL;
    
    int ret, ch;
    
    program_option_add(&input, 'd', "data", 1, "input data to convert");
    program_option_add(&output_format, 'f', "format", 1, "output format");
    program_option_interpret(&argc, &argv);
    
    if (output_format==NULL)  EXIT_ON_ERROR("You need to precise the ouput format -f<format> \n'Y800': 8 bit grey scale");
    else output_pixel_format = STRING_TO_UINT32(output_format);
    
    atexit(quit);
    
    if (input==NULL)
    {
        ret = scanf( "%s", channel_name);
        if (ret != 1) EXIT_ON_ERROR("Fail reading channel name");
    }
    else strncpy(channel_name, input, NAME_MAX);
    
    ret = sscanf(channel_name, "%[^.]", data_name);
    if (ret != 1) EXIT_ON_SYSTEM_ERROR("Read %d fields instead of 1 in '%s' for data '%s'", ret, channel_name, data_name);
    
    data_format = strstr(channel_name, ".tbi");
    if (data_format==NULL) EXIT_ON_ERROR("The channel '%s' does not have a image format extension: .tbi... (type,bytes,image)", channel_name);
    else data_format++; //We do not want keep the dot
    ret=sscanf(data_format, "tbi%4cw%dh%d\n",  (char*)&input_pixel_format, &width, &height);
    if (ret != 3) EXIT_ON_SYSTEM_ERROR("Read %d fields instead of 3 in '%s' for variable '%s'", ret, channel_name, data_name);
    input_channel = new blc_channel(channel_name, 0, "p");
    
    
    if (input_pixel_format==STRING_TO_UINT32("yuv2"))
    {
        snprintf(channel_name, NAME_MAX, "%s.tbi%.4sw%dh%d", data_name, output_format, width, height);
        output_channel = new blc_channel(channel_name, width*height, "p");
        printf("%s\n", channel_name);
    }
    else EXIT_ON_ERROR("Pixel format '%4c' is not managed", (char*)&input_pixel_format);
    
    while(1)
    {
//        SYSTEM_ERROR_CHECK(sem_wait(channel->sem_block), -1, "");
        ch = getc(stdin);
        switch  (ch)
        {
            case 'i':printf("i");fflush(stdout);break;
            case 'q':
                printf("q");
                fflush(stdout);
                exit(0);
                break;
        }
    input_channel->protect();
    output_channel->protect();
    copy_image(input_channel,  input_pixel_format, output_channel, output_pixel_format);
    output_channel->protect(0);
    input_channel->protect(0);

        
        printf(".");
        fflush(stdout);
//       SYSTEM_ERROR_CHECK(sem_post(channel->sem_ack), -1, "");
    }
    
    return 0;
}
