# Set the minimum version of cmake required to build this project
cmake_minimum_required(VERSION 2.6)

# Set the name of the project as the directory basename
get_filename_component(PROJECT_NAME ${CMAKE_SOURCE_DIR} NAME)
project(${PROJECT_NAME})
add_definitions(-Wextra -Wall)

get_filename_component(blaa_dir ${CMAKE_SOURCE_DIR} PATH)
get_filename_component(blaa_build_dir ${CMAKE_BINARY_DIR} PATH)
get_filename_component(blaa_dir ${blaa_dir} PATH)
get_filename_component(blaa_build_dir ${blaa_build_dir} PATH)

#We set blc  as source of the project. This is only useful for developer of blc. However, it defines shared_blc.
add_subdirectory(${blaa_dir}/blc ${blaa_build_dir}/blc_build EXCLUDE_FROM_ALL)
set(EXECUTABLE_OUTPUT_PATH  ${blaa_build_dir}/bin)

include_directories(${blaa_dir}/blc/include)
add_executable(${PROJECT_NAME} src/main.cpp)
target_link_libraries(${PROJECT_NAME} shared_blc)

# *** install ***
install(TARGETS ${PROJECT_NAME} DESTINATION bin)





