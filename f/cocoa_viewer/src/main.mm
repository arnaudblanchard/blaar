//
//  Created by Arnaud Blanchard on 06/11/14.
//  Copyright ETIS 2014. All rights reserved.
//

#include "blc.h"
#include <sys/time.h>
#import <Cocoa/Cocoa.h>


blc_channel *channel;
char channel_name[NAME_MAX];
char data_name[NAME_MAX];
char const *data_input=NULL;
CVPixelBufferRef pixel_buffer;

uchar *tmp_pixels =NULL;
size_t tmp_pixels_size=0;

int initialised_archi = 0;
NSWindow *window;
NSImageView *image_view;
NSCIImageRep *imageRep;
NSImage *image;
OSType pixel_format;


@interface cocoa_vision : NSObject <NSApplicationDelegate>
{}
@end


void *refresh_image(void *user_data)
{
    int ch=0;
    size_t i;
    (void)user_data;
    while(ch!='q')
    {
        if (pixel_format==STRING_TO_UINT32("Y800")) FOR_INV(i, channel->size) tmp_pixels[i*2+1]=channel->data[i];

        imageRep = [[NSCIImageRep imageRepWithCIImage:[CIImage imageWithCVImageBuffer:pixel_buffer]] autorelease];
        image = [[[NSImage alloc] initWithSize:[imageRep size]] autorelease];
        [image setCacheMode:NSImageCacheNever];
        [image addRepresentation:imageRep];
        [image_view setImage:image];
        ch=getchar();
      }
    [NSApp terminate:nil];
    return NULL;
}


@implementation cocoa_vision:NSObject;

- (void)applicationWillFinishLaunching:(NSNotification *)aNotification
{
    NSMenu *mainMenu = [NSMenu alloc];
    (void)aNotification;
    
    id submenu = [NSMenu alloc];
    [submenu addItemWithTitle:@"Quit" action:@selector(terminate:) keyEquivalent:@"q"];
    [mainMenu initWithTitle:@"applicationMenu"];
    [NSApp setMainMenu:mainMenu];
    image_view = [[NSImageView alloc] initWithFrame:NSMakeRect(0, 0, 320, 270)];
    [image_view setImageScaling:NSImageScaleAxesIndependently];
    [window setContentView:image_view];
}

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    int width, height, ret;
    char *data_format;
    pthread_t thread;
    size_t i;
    (void)aNotification;
    
    
    if (data_input==NULL) {
        FSCANF(1, stdin, "%s", channel_name);
    }
    else strncpy(channel_name, data_input, NAME_MAX);
    
    data_format = strstr(channel_name, ".tbi");
    if (data_format==NULL) EXIT_ON_ERROR("The channel '%s' does not have a image format extension: .tbi... (type,bytes,image)", channel_name);
    else data_format++; //We do not want keep the dot
    ret=sscanf(data_format, "tbi%4cw%dh%d\n",  (char*)&pixel_format, &width, &height);
    if (ret != 3) EXIT_ON_SYSTEM_ERROR("Read %d fields instead of 3 in '%s' for variable '%s'", ret, channel_name, data_name);
    channel = new blc_channel(channel_name);
    if (pixel_format==STRING_TO_UINT32("Y800"))
    {
        tmp_pixels_size = 2*width*height;
        tmp_pixels = MANY_ALLOCATIONS(tmp_pixels_size, uchar);
        FOR_INV(i, tmp_pixels_size) tmp_pixels[i]=128;
        
        if (pixel_format==STRING_TO_UINT32("Y800"))
        ret = CVPixelBufferCreateWithBytes(NULL, width, height, STRING_TO_UINT32("yuv2"), tmp_pixels, width*2, NULL,  NULL, NULL, &pixel_buffer);
    }
    else ret = CVPixelBufferCreateWithBytes(NULL, width, height, pixel_format, channel->data, width*2, NULL,  NULL, NULL, &pixel_buffer);
    if (ret != kCVReturnSuccess)
    {
        if (ret == kCVReturnInvalidPixelFormat) EXIT_ON_ERROR("kCVError:%d. '%.4s' is not a valid pixel format", ret, (char*)&pixel_format);
        else EXIT_ON_ERROR("kCVError:%d.", ret);
    }
    [window setTitle:[NSString stringWithFormat:@"%s", channel_name]];

    pthread_create(&thread, NULL, refresh_image, NULL);
}



@end

int main(int argc, char *argv[])
{
    program_option_add(&data_input, 'd', "data", 1, "input data to display");
    program_option_interpret(&argc, &argv);
	
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
    [NSApplication sharedApplication];
	[NSApp setActivationPolicy:NSApplicationActivationPolicyRegular]; //does not work on SDK 10.5
	[NSApp activateIgnoringOtherApps:YES];  
	[NSApp setDelegate:[cocoa_vision new]];
	
	window = [[NSWindow alloc] initWithContentRect:NSMakeRect(20, 20, 320, 270) styleMask:NSTitledWindowMask | NSResizableWindowMask backing:NSBackingStoreBuffered defer:NO];
	[window makeKeyAndOrderFront:window];
	[window setShowsResizeIndicator:YES];
	[window setOpaque:YES];
	[NSApp run];
	[pool drain];

	return 0;
}
