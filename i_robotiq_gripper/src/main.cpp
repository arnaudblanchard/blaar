/*
 *  Created on: June 3, 2015
 *      Author: Julien Dupeyroux
 * 		Based on Sylvain Mahe first work.
 */


#include <stdio.h>
#include <stdlib.h>
#include <modbus.h>
#include <errno.h>
#include <pthread.h>
#include <unistd.h>
#include "blc.h"

#define GRIPPER_ACTIVATED 3
#define GRIPPER_IO_RATE 10000 ////IO period in micro-seconds
#define MOTORS_NB 4


typedef enum {
				GRIPPER_MODE_BASIC = 0,
				GRIPPER_MODE_PINCH = 1,
				GRIPPER_MODE_WIDE = 2,
				GRIPPER_MODE_SCISSOR = 3
			 } GripperMode;

typedef enum {
				GRIPPER_FINGER_A = 0,
				GRIPPER_FINGER_B = 1,
				GRIPPER_FINGER_C = 2,
				GRIPPER_SCISSOR = 3
			} GripperFinger;


/*====GRIPPER STRUCTURE=====*/
typedef struct{
	modbus_t          *ctx;


	uint16_t     __options;

	uint16_t     activated;
	GripperMode    op_mode;
	int         individual;


	uint8_t         pos[4];
	uint16_t  __buf_pos[7];
	uint8_t     __buf_flag;

	uint8_t 	current[4];

	int       request_quit;
	pthread_t    io_thread; // une fct qui marche en paralelle
	pthread_mutex_t io_mut;// combien de prs utilise une ressource


} Gripper;//NOM DE LA CLASSE
/*=========================*/

Gripper * gripper_connect();

void gripper_disconnect(Gripper *gripper);
void gripper_init(Gripper * gripper);
void gripper_flush(Gripper * gripper);

void gripper_set_mode(Gripper *gripper, GripperMode mode);
void gripper_set_individual_control(Gripper *gripper, int individual);

GripperFinger equivalence_int_Finger(int ind);


void  gripper_request_position(Gripper *gripper, GripperFinger finger, int pos, int torques, int speed);

void * __gripper_io(void * data);
void __gripper_io_write(Gripper *gripper);
void __gripper_io_read(Gripper *gripper);

void __gripper_write_options(Gripper *gripper);

void __gripper_request_position_A(Gripper *gripper,  int pos, int torques, int speed);
void __gripper_request_position_B(Gripper *gripper,  int pos, int torques, int speed);
void __gripper_request_position_C(Gripper *gripper,  int pos, int torques, int speed);
void __gripper_request_position_S(Gripper *gripper,  int pos, int torques, int speed);

/*==========GRIPPER INIT============*/
Gripper * gripper_connect(char const *ip_address)
{
	Gripper * g = (Gripper*) malloc(sizeof(Gripper));
	//Modbus Connection
	g->ctx = modbus_new_tcp(ip_address, 502);
	g->activated = 0;
	if (modbus_connect(g->ctx) == -1)
	{
		fprintf(stderr, "Connection failed: %s\n", modbus_strerror(errno));
		gripper_disconnect(g);
		return NULL;
	}
	return g;
}

void gripper_disconnect(Gripper *gripper)
{
	modbus_write_register(gripper->ctx, 0, 0);
	while(gripper->activated == GRIPPER_ACTIVATED) usleep(GRIPPER_IO_RATE);
	modbus_close(gripper->ctx);
	modbus_free(gripper->ctx);
	free(gripper);
}


/*void gripper_init(Gripper* gripper)
{

	gripper->__options = 256;
	__gripper_write_options(gripper);

	int i;
	for(i = 0; i<7; i++) gripper->__buf_pos[i] = 0;

	//Launch IO Thread
	pthread_mutex_init(&(gripper->io_mut), NULL);
	if(pthread_create(&(gripper->io_thread), NULL, __gripper_io, (void*) gripper) != 0)
	{
		fprintf(stderr, "Cannot launch IO Thread ! \n");
		gripper_disconnect(gripper);
	}

	while(gripper->activated != GRIPPER_ACTIVATED) usleep(GRIPPER_IO_RATE);

}*/
void gripper_init(Gripper * gripper){


	uint16_t r0;
	modbus_read_input_registers(gripper->ctx, 0,1,&r0);
	gripper->activated = (r0 >> 12) & 0x3;

	gripper->__options = 256;
	if(!gripper->activated){
		__gripper_write_options(gripper);
	}

	int i;
	for(i = 0; i<7; i++) gripper->__buf_pos[i] = 0;

	//Launch IO Thread
	pthread_mutex_init(&(gripper->io_mut), NULL);
	if(pthread_create(&(gripper->io_thread), NULL, __gripper_io, (void*) gripper) != 0){
		fprintf(stderr, "Cannot launch IO Thread ! \n");
		gripper_disconnect(gripper);
	}

	while(gripper->activated != GRIPPER_ACTIVATED) usleep(GRIPPER_IO_RATE);

	printf("Gripper initialization successful\n");

}

/*================================================*/

/*==============GRIPPER OPTIONS===================*/
void __gripper_write_options(Gripper *gripper)
{
	modbus_write_register(gripper->ctx, 0, gripper->__options);
}

void gripper_set_mode(Gripper *gripper, GripperMode mode)
{
	gripper->__options |= (mode << 9);
	__gripper_write_options(gripper);
}

void gripper_set_individual_control(Gripper *gripper, int individual)
{

	//set or unset individual mode for fingers and scissor
	if(individual) gripper->__options |= (3 << 2);
	else           gripper->__options &= 0xFFF3;

	//set GoTo requested position
	gripper->__options |= (1 << 11);

	//send options to the gripper
	__gripper_write_options(gripper);

	gripper->individual = individual;
}
/*=================================================*/

/*===========IO THREAD==============*/
void * __gripper_io(void * data)
{

	Gripper *gripper = (Gripper*) data;

	while(!gripper->request_quit)
	{

		__gripper_io_write(gripper);
		__gripper_io_read(gripper);

		//wait 'til next update
		usleep(GRIPPER_IO_RATE);
	}
	 return NULL;

}

void __gripper_io_write(Gripper *gripper)
{

	pthread_mutex_lock(&(gripper->io_mut));
	if(gripper->__buf_flag)
	{
		modbus_write_registers(gripper->ctx, 1, 8, gripper->__buf_pos);
		gripper->__buf_flag = 0;
	}
	pthread_mutex_unlock(&(gripper->io_mut));
}


void __gripper_io_read(Gripper *gripper)
{

	uint16_t registers[8];

	//read status
	modbus_read_input_registers(gripper->ctx, 0, 8, registers);

	/*======update status======*/

	//Is activated ?
	gripper->activated = (registers[0] >> 12) & 0x3;

	//Gripper operation mode
	gripper->op_mode = (GripperMode)((registers[0] >> 9) & 0x3);

	//Fingers pos
	gripper->pos[GRIPPER_FINGER_A] = registers[2] >> 0x08;
	gripper->pos[GRIPPER_FINGER_B] = registers[3] &  0xFF;
	gripper->pos[GRIPPER_FINGER_C] = registers[5] >> 0x08;
	gripper->pos[GRIPPER_SCISSOR]  = registers[6] &  0xFF;

}

/*void __gripper_io_read(Gripper *gripper){

	uint16_t registers[8];

	//read status
	modbus_read_input_registers(gripper->ctx, 0, 8, registers);

	/*======update status=======*/

	//Is activated ?
	/*gripper->activated = (registers[0] >> 12) & 0x3;

	//Gripper operation mode
	gripper->op_mode = (GripperMode)((registers[0] >> 9) & 0x3);

	//Fingers pos
	gripper->pos[GRIPPER_FINGER_A] = registers[2] >> 0x08;
	gripper->pos[GRIPPER_FINGER_B] = registers[3] &  0xFF;
	gripper->pos[GRIPPER_FINGER_C] = registers[5] >> 0x08;
	gripper->pos[GRIPPER_SCISSOR]  = registers[6] &  0xFF;

	gripper->current[GRIPPER_FINGER_A] = registers[2] &  0xFF;
	gripper->current[GRIPPER_FINGER_B] = registers[4] >> 0x08;
	gripper->current[GRIPPER_FINGER_C] = registers[5] &  0xFF;
	gripper->current[GRIPPER_SCISSOR]  = registers[7] >> 0x08;

}
/*======================================*/

void gripper_flush(Gripper *gripper)
{
	__gripper_io_write(gripper);
}
/*======================================*/

GripperFinger equivalence_int_Finger(int ind)
{
	switch(ind){
		case 0: return GRIPPER_FINGER_A; break;
		case 1: return GRIPPER_FINGER_B; break;
		case 2: return GRIPPER_FINGER_C; break;
		case 3: return GRIPPER_SCISSOR;  break;
		default : break;
	}

}

/*=============POSITION, SPEED & torques REQUEST HANDLING===============*/
void gripper_request_position(Gripper *gripper, GripperFinger finger, int pos, int torques, int speed)
{

	pthread_mutex_lock(&(gripper->io_mut));
	switch(finger)
	{
		case GRIPPER_FINGER_A: __gripper_request_position_A(gripper, pos,torques,speed); break;
		case GRIPPER_FINGER_B: __gripper_request_position_B(gripper, pos,torques,speed); break;
		case GRIPPER_FINGER_C: __gripper_request_position_C(gripper, pos,torques,speed); break;
		case GRIPPER_SCISSOR:  __gripper_request_position_S(gripper, pos,torques,speed); break;
		default: break;
	}
	gripper->__buf_flag = 1;
	pthread_mutex_unlock(&(gripper->io_mut));

}
void __gripper_request_position_A(Gripper *gripper,  int pos, int torques, int speed)
{
	if(pos >= 0 && pos <= 255) 	   gripper->__buf_pos[0] = (uint8_t) pos;
	if(speed >= 0 && speed <= 255) gripper->__buf_pos[1] = (gripper->__buf_pos[1] & 0x00FF) | (((uint8_t) speed) << 8);
	if(torques >= 0 && torques <= 255) gripper->__buf_pos[1] = (gripper->__buf_pos[1] & 0xFF00) | ((uint8_t) torques);
}
void __gripper_request_position_B(Gripper *gripper,  int pos, int torques, int speed)
{
	if(pos >= 0 && pos <= 255)     gripper->__buf_pos[2] = (gripper->__buf_pos[2] & 0x00FF) | (((uint8_t) pos) << 8);
	if(speed >= 0 && speed <= 255) gripper->__buf_pos[2] = (gripper->__buf_pos[2] & 0xFF00) | ((uint8_t) speed);
	if(torques >= 0 && torques <= 255) gripper->__buf_pos[3] = (gripper->__buf_pos[3] & 0x00FF) | (((uint8_t) torques) << 8);
}
void __gripper_request_position_C(Gripper *gripper,  int pos, int torques, int speed)
{
	if(pos >= 0 && pos <= 255)	   gripper->__buf_pos[3] = (gripper->__buf_pos[3] & 0xFF00) | ((uint8_t) pos);
	if(speed >= 0 && speed <= 255) gripper->__buf_pos[4] = (gripper->__buf_pos[4] & 0x00FF) | (((uint8_t) speed) << 8);
	if(torques >= 0 && torques <= 255) gripper->__buf_pos[4] = (gripper->__buf_pos[4] & 0xFF00) | ((uint8_t) torques);
}
void __gripper_request_position_S(Gripper *gripper,  int pos, int torques, int speed)
{
	if(pos >= 0 && pos <= 255)     gripper->__buf_pos[5] = (gripper->__buf_pos[5] & 0x00FF) | (((uint8_t)pos) << 8);
	if(speed >= 0 && speed <= 255) gripper->__buf_pos[5] = (gripper->__buf_pos[5] & 0xFF00) | ((uint8_t) speed);
    if(torques >= 0 && torques <= 255) gripper->__buf_pos[6] = (gripper->__buf_pos[6] & 0x00FF) | (((uint8_t)torques) << 8);
}
/*=============================================================================================*/

/////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////// GESTION ORDRES ENVOYES A LA PINCE /////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////

int main(int argc, char**argv)
{

	int i;

	double neural_position, neural_torques, neural_speed;
	char const *ip_address;
	const char *broker_name;



	neural_position = 0.0;
	neural_torques  = 1.0;
	neural_speed    = 1.0;

	blc_channel set_positions_channel,get_positions_channel, get_torques_channel, set_torques_channel, set_speed_channel, get_speed_channel;
	float *set_positions_values, *get_positions_values, *get_torques_values, *set_torques_values, *set_speed_values,*get_speed_values;

	 blc_program_set_description("Display the content of the blc_channel depending on its type and format");
	 blc_program_option_add(&ip_address, 'i', "ip", "string", "hand ip", "192.168.1.11");
	 blc_program_option_add(&broker_name, 'b', "broker", "string", "broker_name", "hand");
	 blc_program_init(&argc, &argv, NULL);

	/* Connexion à la pince */
	printf("Connecting to the gripper ...\n");
	Gripper *gripper = gripper_connect(ip_address);
	if(gripper == NULL) EXIT_ON_ERROR("Gripper not connected");

	else {printf("Gripper connected\n");}
	/* Initialisation de la pince */
	gripper_init(gripper);
	printf("Initialization of RobotiQ gripper completed.\n");
	/* Gestion du mode */
	gripper_set_mode(gripper,GRIPPER_MODE_BASIC);
	gripper_set_individual_control(gripper,TRUE);
	/* Envoi de commandes de mise en position initiale (tout à 0 -> pince totalement ouverte) */
	printf("Setting starting positions\n");
	printf("   1st order : GRIPPER_FINGER_A\n");
	gripper_request_position(gripper, equivalence_int_Finger(1), 0,255,255);
	printf("   2nd order : GRIPPER_FINGER_B\n");
	gripper_request_position(gripper, equivalence_int_Finger(2), 0,255,255);
	printf("   3rd order : GRIPPER_FINGER_C\n");
	gripper_request_position(gripper, equivalence_int_Finger(3), 0,255,255);
	printf("   4th order : GRIPPER_SCISSOR\n");
	gripper_request_position(gripper, equivalence_int_Finger(0), 0,255,255);
	printf("Gripper is now on starting position.\n");



	/** BLC::positions */
	set_positions_channel.set_namef("/%s.set_positions", broker_name);
	set_positions_channel.create_or_open(BLC_SEM_PROTECT, 'FL32', 'NDEF', NULL, 1, MOTORS_NB); // void create_or_open(int sem_abp, uint32_t type, uint32_t format,  char const *parameter, int dims_nb, int length0, ...);
	set_positions_values=set_positions_channel.floats;

	get_positions_channel.set_namef("/%s.get_positions", broker_name);                 //blc_channel(channel_name, sizeof(float)*MOTORS_NB);
	get_positions_channel.create_or_open(BLC_SEM_PROTECT, 'FL32', 'NDEF', NULL, 1, MOTORS_NB);
	get_positions_values=get_positions_channel.floats;

	/** BLC::torques */

	set_torques_channel.set_namef("/%s.set_torques", broker_name);
	set_torques_channel.create_or_open (BLC_SEM_PROTECT, 'FL32', 'NDEF', NULL, 1, MOTORS_NB);
	set_torques_values=set_torques_channel.floats;


	get_torques_channel.set_namef("/%s.get_torques", broker_name);
	get_torques_channel.create_or_open (BLC_SEM_PROTECT, 'FL32', 'NDEF', NULL, 1, MOTORS_NB);
	get_torques_values=get_torques_channel.floats;

	/** BLC::SPEED */

	set_speed_channel.set_namef("/%s.set_speed", broker_name);
	set_speed_channel.create_or_open(BLC_SEM_PROTECT, 'FL32', 'NDEF', NULL, 1, MOTORS_NB);
	set_speed_values=set_speed_channel.floats;


	get_speed_channel.set_namef("/%s.get_speed", broker_name);
	get_speed_channel.create_or_open (BLC_SEM_PROTECT, 'FL32', 'NDEF', NULL, 1, MOTORS_NB);
	get_speed_values=get_speed_channel.floats;


	while(1)
	{
		for(i=0;i<MOTORS_NB;i++)
		{
			/* Get Data */
			get_positions_values[i]=(float)(gripper->pos[i])/256.;
			get_torques_values[i]=(float)(gripper->current[i])/256.;

			/* Set Data */
	//set_torques_values=(float*)set_torques_channel.data;
	//printf("Torques n%d : %f\n",i,(double)(set_torques_values[i]));
	//set_speed_values=(float*)set_speed_channel.data;
			gripper_request_position(gripper, equivalence_int_Finger(i), (int)(255*set_positions_values[i]), /*255 -*/ (int)(255*set_torques_values[i]), 255 /*- (int)(255*set_speed_values[i])*/);
		}
	}

	printf("Gripper disconnecting...\n");
	gripper_disconnect(gripper);

 return 0;
}
