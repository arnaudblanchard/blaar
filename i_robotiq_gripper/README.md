Pour utiliser la pince 3 doigts Robotiq : 
	- connexion par câble ethernet : créer un nouveau profil ethernet dont les paramètres sont les suivants : 
	IPV4 >> Manuel >> Paramètres : 
				adresse -> 192.168.1.10
				netmask -> 255.255.255.0
				passerelle -> 0.0.0.0
	- utilisation du broker : 
	corriger le path d'accès à libblc.a  (pour moi, c'est : 
		/home/juliendupeyroux/simulateur/lib/Linux/blc/libblc.a)
	de même, dans gripper.cpp, changer les paths d'accès aux deux librairies blc_tools.h et  blc_channel.h
	- compiler le broker (> make)
	- lancer le broker   (> ./gripper)
	- dans la mémoire partagée, vous trouverez alors : 
prom.robotiq.gripper.get_positions-tfv  prom.robotiq.gripper.set_positions-tfv  
prom.robotiq.gripper.get_speed-tfv      prom.robotiq.gripper.set_speed-tfv   
prom.robotiq.gripper.get_torques-tfv    prom.robotiq.gripper.set_torques-tfv
	- et voilà !

http://support.robotiq.com/display/RUI/6.2+Modbus+TCP 

