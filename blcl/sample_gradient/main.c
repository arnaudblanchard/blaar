#include "blc.h"
#include "blcl.h"
#include <sys/stat.h> //Size of th file
#include <libgen.h>

#define FILENAME "lenna.jpg"


int main(int argc, char **argv)
{
    FILE *file;
    struct stat stat_data;
    blc_narray jpeg;
    blc_channel channel;
    
    SYSTEM_ERROR_CHECK(stat(FILENAME, &stat_data), -1, "Finding '%s'", FILENAME);
    SYSTEM_ERROR_CHECK(file=fopen(FILENAME, "rb"), NULL, "Failing opening image '%s'", FILENAME);
    blc_mem_allocate(&jpeg.mem, stat_data.st_size);
    SYSTEM_SUCCESS_CHECK(fread(jpeg.mem.data, 1, jpeg.mem.size, file), jpeg.mem.size, NULL);
    SYSTEM_SUCCESS_CHECK(fclose(file), 0, NULL);
    
    jpeg.type='UIN8';
    jpeg.format='JPEG';
    
    blc_narray_jpeg_init(&channel.info.narray, &jpeg);
    blc_image_copy(&channel.info.narray, &jpeg);
    blc_narray_destroy(&jpeg);

    blc_channel_create_or_open(&channel, "/sample_gradient", BLC_SEM_NONE, NULL);
    
    printf("toto\n");
    
    return 0;
}

