#include "blc.h"

void change_text_cb(char const *command, char const *argument, char **text){
    (void)command;
    
    free(*text);
    *text=strdup(argument);
}
 
int main(int argc, char **argv){
    char const *initial_text,  *flag;
    char *text;
    
    //Set the description of the program (used while executing with --help flag).
    blc_program_set_description("Example of a simple textual program. This show how to use parser and keyboard interaction");
    //parameter allowing to set a text
    blc_program_option_add(&initial_text, 't', "text", "string", "classic string", "default text");
    //flag is containing either NULL or "1" if it is set.
    blc_program_option_add(&flag, 'f', "flag", NULL, "test a binary flag", NULL);
    //Display the title, interpret the arguments, set the quitting function.
    blc_program_init(&argc, &argv, NULL);
    //copy the text that was in argv
    text=strdup(initial_text);
    
    //Add command to be executed interactively
    blc_command_add("t", (type_blc_command_cb)change_text_cb, "new text", "the new text to replace", &text);
    
    //Loop until 'q' is sent. It also allows to make statistics on the time with 's', display help with 'h' and pause with return.
    BLC_PROGRAM_FOR_LOOP(){
        fprintf(stderr, "I repeat loop here until quitting. The text is: '%s'\n", text);
        sleep(1);
    }
	return 0;
}


