#include "blc.h"


blc_channel channel;
#define SIZE 32

void new_text_cb(char const *, char const *argument, void *){
    
    strncpy(channel.data, argument, SIZE);
}

void on_quit(void){
    
    blc_channel tmp_channel;
    if (blc_channel_get_info_with_name(&tmp_channel, channel.name) != -1)  fprintf(stderr, "You may want to destroy the created channel:\n./run.sh blc_channels -u/channel_example\n");
}

int main(int argc, char **argv){
    int created;
    blc_program_set_description("Example of a program writing and reading a channel.");
    //Display the title, interpret the arguments, set the quitting function.
    blc_program_init(&argc, &argv, on_quit);
  
    created=channel.create_or_open("/channel_example", 0, 'UIN8', 'NDEF', NULL, 1, SIZE);
    
    if (created) fprintf(stderr, "Open a new terminal and execute the same command to share the memory\n\n");
    
    blc_command_add("t", new_text_cb, "new text", "text to put in shared memeory", NULL);
    BLC_PROGRAM_FOR_LOOP(){
        
        fprintf(stderr, "common text: '%*s'\n", SIZE, channel.data);
        sleep(1);
        
        
    }
}