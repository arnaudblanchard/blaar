#include "blc.h"
#include <math.h>

#define WIDTH 60
#define HEIGHT 20

int main(int argc, char **argv){
    int i=0, k=0;
    blc_mem mem;
    char const *gain;
    float g;
    
    //Set the description of the program (used while executing with --help flag).
    blc_program_set_description("Example of a simple textual graphic");
    blc_program_option_add(&gain, 'g', "gain", "real", "gain to slow down or accelerate", "0.02");
    //Display the title, interpret the arguments, set the quitting function.
    blc_program_init(&argc, &argv, NULL);
    //Convert the text in float. There is no check if it is not a real number.
    g=strtof(gain, NULL);

    //Make sur mem.data is unitilised
    CLEAR(mem);
    blc_mem_allocate(&mem, WIDTH);
    BLC_PROGRAM_FOR_LOOP(){
        //Define the data to graph (sinus function)
        FOR(i, WIDTH)  mem.uints8[i]=(sin((i+k)*g)+1)*128;
        //Display the graph.
        fprint_graph(stderr,  &mem, "sinus", HEIGHT, 256, 0,  "time", "intensity" );
        //Go up to display the next graph in the same place.
        fprintf_escape_command(stderr, MOVE_N_UP_CURSOR, HEIGHT);
        k++;
    }
    return 0;
}