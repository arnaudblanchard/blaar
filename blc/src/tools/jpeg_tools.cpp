/*
 Basic Library for C/C++ (blclib)
 Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (2011 - 2015)
 
 Author:  Arnaud Blanchard
 
 This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
 You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
  users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
  In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
  that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
 Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
  and, more generally, to use and operate it in the same conditions as regards security.
  The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms. *
 
 *
 *
 *  Created on: August 1, 2015
 *      Author: Arnaud Blanchard
 */
#include "jpeg_tools.h"
#include "blc.h"

#ifdef JPEGLIB
static void jpeg_error(j_common_ptr cinfo, int msg_level)
{
    (void) msg_level;
    cinfo->err->num_warnings++;
}

void blc_narray_jpeg_init(blc_narray *image, blc_narray const *mem)
{
    struct jpeg_decompress_struct cinfo;
    struct jpeg_error_mgr jerr;
    
    cinfo.err = jpeg_std_error(&jerr);
    cinfo.err->emit_message = jpeg_error;
    jpeg_create_decompress(&cinfo);
    jpeg_mem_src(&cinfo, (uchar*)mem->data, mem->size);
    jpeg_read_header(&cinfo, TRUE);
    
    image->type='UIN8';
    switch (cinfo.out_color_space){
        case JCS_GRAYSCALE:
            image->format='Y800';
            break;
        case JCS_RGB:
            image->format='RGB3';
            image->add_dim(3);
            break;
        default:EXIT_ON_ERROR("out_color_space '%d' is not yet managed in jpeg.", cinfo.out_color_space);
            break;
    }
    image->add_dim(cinfo.image_width);
    image->add_dim(cinfo.image_height);
    image->allocate();
    jpeg_destroy_decompress(&cinfo);
}

void blc_jpeg_read(blc_narray *dest, blc_narray const *jpeg)
{
    JSAMPROW row_pt[1];
    int row_stride;
    struct jpeg_decompress_struct cinfo;
    struct jpeg_error_mgr jerr;
    
    cinfo.err = jpeg_std_error(&jerr);
    cinfo.err->emit_message = jpeg_error;
    jpeg_create_decompress(&cinfo);
    jpeg_mem_src(&cinfo, (uchar*)jpeg->data, jpeg->size);
    jpeg_read_header(&cinfo, TRUE);
    jpeg_start_decompress(&cinfo);
    row_stride = cinfo.output_width * cinfo.num_components;
    row_pt[0] = (uchar*)dest->data;
    
    while (cinfo.output_scanline < cinfo.output_height)
    {
        jpeg_read_scanlines(&cinfo, row_pt, 1);
        row_pt[0] += row_stride;
    }
    
    if (cinfo.err->num_warnings != 0) PRINT_WARNING("jpeglib %d warnings. Last one : %s", cinfo.err->num_warnings, cinfo.err->jpeg_message_table[cinfo.err->last_jpeg_message]);
    jpeg_finish_decompress(&cinfo);
    jpeg_destroy_decompress(&cinfo);
}

void blc_jpeg_write(blc_narray *jpeg, blc_narray const *src)
{
    JSAMPROW row_pt[1];
    int row_stride;
    struct jpeg_compress_struct cinfo;
    struct jpeg_error_mgr jerr;
    uint32_t format_str;
    
    cinfo.err = jpeg_std_error(&jerr);
    cinfo.err->emit_message = jpeg_error;
    jpeg_create_compress(&cinfo);
    jpeg_mem_dest(&cinfo, (uchar**)&jpeg->data, &jpeg->size);
    cinfo.image_width=src->lengths[src->dims_nb-2];
    cinfo.image_height=src->lengths[src->dims_nb-1];
    switch (src->format){
        case 'Y800':
            cinfo.input_components=1;
            cinfo.in_color_space=JCS_GRAYSCALE;
            break;
        case 'RGB3':
            cinfo.input_components=3;
            cinfo.in_color_space=JCS_RGB;
            break;
        default:EXIT_ON_ERROR("format '%.4s' is not yet managed.", UINT32_TO_STRING(format_str, src->format));
            break;
    }
    jpeg_set_defaults(&cinfo);
    jpeg_start_compress(&cinfo, TRUE);
    row_stride=cinfo.input_components*cinfo.image_width;
    row_pt[0] = (uchar*)jpeg->data;
    
    while (cinfo.next_scanline < cinfo.image_height) {
        jpeg_write_scanlines(&cinfo, row_pt, 1);
        row_pt[0]+=row_stride;
    }
    
    if (cinfo.err->num_warnings != 0) PRINT_WARNING("blc %d errors. Last one : %s", cinfo.err->num_warnings, cinfo.err->jpeg_message_table[cinfo.err->last_jpeg_message]);
    jpeg_finish_compress(&cinfo);
    jpeg_destroy_compress(&cinfo);
}
#else
void blc_jpeg_write(blc_narray *, blc_narray const *)
{
    EXIT_ON_ERROR("You need jpeglib");
}
void blc_jpeg_read(blc_narray *, blc_narray const *)
{
    EXIT_ON_ERROR("You need jpeglib");
}
#endif






