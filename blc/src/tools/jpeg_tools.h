
#include "blc_mem.h"
#ifdef JPEGLIB
#include <jpeglib.h>
#endif

void blc_jpeg_write(blc_narray *jpeg, blc_narray const *src);
void blc_jpeg_read(blc_narray *dest, blc_narray const *jpeg);
