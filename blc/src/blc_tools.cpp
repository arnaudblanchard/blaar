/* Basic Library for C/C++ (blclib)
 Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (2011 - 2014)
 
 Author: Arnaud Blanchard
 
 This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
 You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
  users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
  In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
  that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
 Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
  and, more generally, to use and operate it in the same conditions as regards security.
  The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms. */

//
//  Created by Arnaud Blanchard on 17/06/2014.
//
//

#include "blc_tools.h"

#include <signal.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <time.h>  //time val localtime
#include <sys/time.h> //gettimeofday
#include <sys/uio.h> //iovec
#include "blc.h"
FILE *blc_log_file = NULL;

char const *blc_program_name="";


size_t blc_get_type_size(uint32_t type)
{
    uint32_t type_str;
    switch (type) {
        case 'UIN8':case 'INT8':return 1;
            break;
        case 'UI16':case 'IN16': return 2;
            break;
        case 'UI32':case 'IN32':case 'FL32': return 4;
            break;
        case 'UI64':case 'IN64':case 'FL64': return 8;
            break;
        default:EXIT_ON_ERROR("Unknonw type '%.4s'.", UINT32_TO_STRING(type_str, type));
            break;
    }
    return 0;
}


//Envoie un message de warning avec name_of_file, name_of_function, number_of_line et affiche le message formate avec les parametres variables. To be used with PRINT_WARNING.
void print_warning(const char *name_of_file, const char* name_of_function, int numero_of_line, const char *message, ...)
{
    va_list arguments;
    va_start(arguments, message);
    color_fprintf(BLC_BRIGHT_YELLOW, stderr, "\n%s: %s \t %s \t %i :\nWarning: ", blc_program_name, name_of_file, name_of_function, numero_of_line);
    color_vfprintf(BLC_BRIGHT_YELLOW, stderr, message, arguments);
    fprintf(stderr, "\n\n");
    fflush(stderr);
    va_end(arguments);
}

//Envoie un message de warning avec name_of_file, name_of_function, number_of_line et affiche le message formate avec les parametres variables. To be used with PRINT_WARNING.
void print_system_error(const char *name_of_file, const char* name_of_function, int numero_of_line, const char *message, ...)
{
    va_list arguments;
    va_start(arguments, message);
    color_fprintf(BLC_RED, stderr, "\n%s: %s \t %s \t %i :\nSystem error: %s\n", blc_program_name, name_of_file, name_of_function, numero_of_line, strerror(errno));
    color_vfprintf(BLC_BRIGHT_YELLOW, stderr, message, arguments);
    fprintf(stderr, "\n\n");
    fflush(stderr);
    va_end(arguments);
}

// Envoie un message d'erreur avec name_of_file, name_of_function, number_of_line et affiche le message formate avec les parametres variables. Puis exit le programme avec le parametre EXIT_FAILURE. To be used with EXIT_ON_ERROR.
void fatal_error(const char *name_of_file, const char* name_of_function, int numero_of_line, const char *message, ...)
{
    va_list arguments;
    va_start(arguments, message);
    color_fprintf(BLC_BRIGHT_RED, stderr, "\n%s: %s \t %s \t %i :\nError: ", blc_program_name, name_of_file, name_of_function, numero_of_line);
    color_vfprintf(BLC_BRIGHT_RED, stderr, message, arguments);
    va_end(arguments);
    fprintf(stderr, "\n\n");
    fflush(stderr);
    raise(SIGABRT);
    exit(EXIT_FAILURE);
}

//Envoie un message d'erreur avec name_of_file, name_of_function, number_of_line et affiche le message formate avec les parametres variables. Puis exit le programme avec le parametre EXIT_FAILURE. To be used with @ref EXIT_ON_SYSTEM_ERROR .
void fatal_system_error(const char *name_of_file, const char* name_of_function, int numero_of_line, const char *message, ...)
{
    va_list arguments;
    va_start(arguments, message);
    color_fprintf(BLC_BRIGHT_RED, stderr, "\n%s: %s \t %i:%s\n", blc_program_name, name_of_file, numero_of_line, name_of_function);
    color_fprintf(BLC_BRIGHT_RED, stderr, "System error: %s\n", strerror(errno));
    color_vfprintf(BLC_BRIGHT_RED, stderr, message, arguments);
    va_end(arguments);
    fprintf(stderr, "\n");
    fflush(stderr);
    raise(SIGABRT);
    exit(EXIT_FAILURE);
}

void fatal_command_system_error(const char *name_of_file, const char* name_of_function, int numero_of_line, char const *command,const char *message, ...)
{
    va_list arguments;
    va_start(arguments, message);
    color_fprintf(BLC_BRIGHT_RED, stderr, "\n%s: %s \t %i:%s\n", blc_program_name, name_of_file, numero_of_line, name_of_function);
    fprintf(stderr, "Executing: %s\n", command);
    color_fprintf(BLC_BRIGHT_RED, stderr, "System error: %s\n", strerror(errno));
    color_vfprintf(BLC_BRIGHT_RED, stderr, message, arguments);
    va_end(arguments);
    fprintf(stderr, "\n\n");
    fflush(stderr);
    raise(SIGABRT);
    exit(EXIT_FAILURE);
}

// Fait un malloc de taille 'size' verifie qu'il n'a pas renvoye NULL et renvoie ce pointeur. Si le malloc a renvoye NULL alors une erreur fatal est generee avec le filename, name_of_function, et line comme parametres.
void *secure_malloc(const char *file, const char *function, int line, size_t size)
{
    void *pointer;
    
    if (size){
        pointer = malloc(size);
        if (pointer == NULL) fatal_error(file, function, line, "malloc of size %d has failed (return NULL)", size);
        else return pointer;
    }
    return NULL;
}

//Fait un realloc de taille 'size' sur le pointeur verifie qu'il n'a pas renvoye NULL et renvoie la nouvelle addresse du pointeur. Si le pointeur d'origine est NULL alors la fonction a le meme effet que secure_malloc. Si le realloc a renvoiye NULL alors une erreur fatal est generee avec le filename, name_of_function, et line comme parametres.
void secure_realloc(const char *file, const char *function, int line, void** pointer, size_t size)
{
    *pointer = realloc(*pointer, size);
    if (*pointer == NULL) if (size != 0) fatal_error(file, function, line, "realloc of size %d has failed (return NULL)", size);
}

void *append_item(const char *file, const char *function, int line, void** pointer, size_t pointer_content_size, int *items_nb, size_t item_size, void* new_item)
{
    void *new_address;
    
    if (pointer_content_size != item_size) fatal_error(file, function, line, "The size '%ld' of the content of the array must be equal to the size '%ld' of the item to contain.\nYou may mix content and pointer.", pointer_content_size, item_size);
    
  /*  if (items_nb==NULL)// In which case
    {
        secure_realloc(file, function, line, (void**) pointer, item_size);
        new_address=*pointer;
    }
    else
    {*/
        secure_realloc(file, function, line, (void**) pointer, item_size * (*items_nb + 1));
        new_address=((char*)*pointer)+(*items_nb) * item_size;
        *items_nb += 1;
    //}
    memcpy(new_address, new_item, item_size);
    return new_address;
}

void *insert_item(const char *file, const char *function, int line, void** pointer, size_t pointer_content_size, int *items_nb, size_t item_size, void* new_item, int position)
{
    char *tmp_pointer;
    
    if (pointer_content_size != item_size) fatal_error(file, function, line, "The size '%ld' of the content of the array must be equal to the size '%ld' of the item to contain.", pointer_content_size, item_size);
    
    secure_realloc(file, function, line, (void**) pointer, item_size * (*items_nb + 1));
    tmp_pointer = (char*)*pointer;
    memmove(tmp_pointer + (position+1) * item_size, tmp_pointer+position*item_size, (*items_nb - position)*item_size);
    memcpy(tmp_pointer+position * item_size, new_item, item_size);
    *items_nb += 1;
    return tmp_pointer+position * item_size;
}

void remove_item_id(const char *file, const char *function, int line, void** pointer, size_t pointer_content_size, int *items_nb, int position)
{
   char *tmp_pointer;

    (*items_nb)--;
    tmp_pointer = (char*)*pointer;

    memcpy(tmp_pointer+pointer_content_size*position, tmp_pointer + pointer_content_size*(*items_nb), pointer_content_size);
    secure_realloc(file, function, line, (void**) pointer, pointer_content_size * (*items_nb));
}


int get_item_position(const char *file, const char *function, int line, void const *const* array, size_t pointer_content_size, int items_nb, size_t item_size, void* researched_item)
{
    int i;
    
    if (pointer_content_size != item_size) fatal_error(file, function, line, "The size '%d' of the content of array must be equal to the size of the researched item '%d'", pointer_content_size, item_size);
    
    FOR_INV(i, items_nb)
    {
        if (memcmp(researched_item, &array[i], item_size) == 0) return i;
    }
    return -1;
}

/* May change */
char rand_char()
{
    return rand() / (((unsigned int) (RAND_MAX) + 1) / 256);
}

/* We use an intermediate buffer line in order to avoid thread mix */
void printf_log(const char *log_name, ...)
{
    const char*format;
    va_list arguments;
    struct timeval tv;
    struct tm *tm;
    time_t nowtime;
    char line[LINE_MAX]; //The buffer in order to avoid concurency problem
    char filename[FILENAME_MAX];
    size_t line_size;
    
    if (blc_log_file == NULL)
    {
        snprintf(filename, FILENAME_MAX, "/tmp/%s%d.log", log_name, getpid());
        blc_log_file = fopen(filename, "w+");
        if (blc_log_file== NULL) EXIT_ON_SYSTEM_ERROR("Creating log file %s", filename);
    }
    
    gettimeofday(&tv, NULL);
    nowtime = tv.tv_sec;
    tm = localtime(&nowtime);
    line_size = strftime(line, LINE_MAX, "[%Hh%Mm%S", tm);
    line_size+=sprintf(line+line_size, ".%.6lds]", (long)tv.tv_usec);
    va_start(arguments, log_name);
    format = va_arg(arguments, const char *);
    line_size+=vsnprintf(line+line_size, LINE_MAX,  format, arguments);
    va_end(arguments);
    line[line_size]='\n';
    if (fwrite(line, line_size+1, 1, blc_log_file) != 1) EXIT_ON_SYSTEM_ERROR("Writing log: %s", line);
    if (fflush(blc_log_file) != 0) EXIT_ON_SYSTEM_ERROR("Flushing log file.");
}

/*copy file to buffer
 
 SYSTEM_ERROR_CHECK(blc_channels_file = fopen("/tmp/blc_channels.list", "r"), NULL, "");
 SYSTEM_ERROR_CHECK(fseek(blc_channels_file, 0 , SEEK_END), -1, "");
 SYSTEM_ERROR_CHECK(file_size = ftell(blc_channels_file), -1, "");
 SYSTEM_ERROR_CHECK(buf = mmap(NULL, file_size, PROT_READ, MAP_PRIVATE, fileno(blc_channels_file), 0 ), MAP_FAILED, "");
 SYSTEM_SUCCESS_CHECK(fwrite(buf,  file_size, 1, stdout),1 , "");
 munmap(buf, file_size);
 fclose(blc_channels_file);
 
 */




