/* Basic Library for C/C++ (blclib)
 Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (2011 - 2014)
 
 Author:  Arnaud Blanchard
 
 This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
 You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
  users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
  In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
  that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
 Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
  and, more generally, to use and operate it in the same conditions as regards security.
  The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms. */

#include "blc_text_graph.h"

#include <stdio.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <pthread.h>
#include <termios.h>
#include <sys/select.h>
#include <getopt.h>
#include "blc.h"

static blc_mem console_buffer;

static char *ansi_texts=NULL;
char *uchar_bar_colors = NULL;
char *file_texts=NULL;

START_EXTERN_C
void fprint_graphs(FILE *file,  blc_mem const *mems, char const *const* title, int graphs_nb, int height, int max, int min,  char const* abscissa_name,  char const* ordinate_name )
{
    unsigned char value;
    size_t i, total_size = 0;
    int range, ordinate_name_length, ordinate_name_position;
    int j, threhold, threhold1, graph_id;
    char c;
    char vertical_arrow[] = "^ ";
    
    ordinate_name_position = - strlen(vertical_arrow);
    
    if (ordinate_name != NULL) ordinate_name_length = strlen(ordinate_name);
    else ordinate_name_length = 0;
    
    height-=3;
    range = max - min + 1;
    
    fprintf(file, "\n");
    FOR(graph_id, graphs_nb)
    {
        for (i = fprintf(file, "%d [%s] ", max, title[graph_id]); i < mems[graph_id].size * 3; ++i) fputc('-', file);
        total_size+=mems[graph_id].size;
        fputc('|', file);
        
    }
    fprintf(file, "\n");
    
    FOR_INV(j, height)
    {
        threhold = j * range / height;
        threhold1 = (j + 1) * range / height;
        if (ordinate_name_position < 0)
        {
            c = vertical_arrow[strlen(vertical_arrow) + ordinate_name_position];
            ordinate_name_position++;
        }
        else if (ordinate_name_position != ordinate_name_length)
        {
            c = ordinate_name[ordinate_name_position];
            ordinate_name_position++;
        }
        else c = '|';
        
        FOR(graph_id, graphs_nb)
        {
            fputc(c, file);
            
            FOR(i, mems[graph_id].size)
            {
                value = mems[graph_id].data[i];
                if (value - min > threhold1) fprintf(file, "[] ");
                else if (value - min >= threhold) fprintf(file, "%2d ", 100 * (value - min) / range);
                else fprintf(file, "   ");
            }
        }
        fprintf(file, "\n");
    }
    FOR(graph_id, graphs_nb)
    {
        for (i = fprintf(file, "%d %s ", min, abscissa_name); i < mems[graph_id].size * 3; ++i)
            fputc('-', file);
        fputc('>', file);
    }
    
    fprintf(file, "\n");
}

void fprint_graph(FILE *file,  blc_mem const *mem, char const *title, int height, int max, int min,  char const* abscissa_name,  char const* ordinate_name ){
    fprint_graphs(file, mem, &title, 1, height, max, min, abscissa_name, ordinate_name);
}


void fprint_matrix_graph(FILE *file, blc_mem *mem, int step, char const *title, int width, int height)
{
    uchar value;
    int color_id;
    int i, j;
    
    fprintf(file, "[%s]", title);
    fprintf_escape_command(file, DEL_END_LINE);
    fprintf(file, "\n");
    
    width=MIN(width/3, step/8);
    height=MIN(height-2, (int)(mem->size/(step*2)));
    
    FOR(j, height*4)
    {
        FOR(i, width)
        {
            value = (uchar)mem->data[j*step+i*8+1]*100/256;
            if ( value < 20) color_id = BLC_BRIGHT_BLUE;
            else if ( value < 35 ) color_id = BLC_BLUE;
            else if ( value < 45 ) color_id = BLC_GREY;
            else if ( value < 55 ) color_id = BLC_WHITE;
            else if ( value < 65 ) color_id = BLC_BRIGHT_WHITE;
            else if ( value < 80 ) color_id = BLC_RED;
            else  color_id = BLC_BRIGHT_RED;
            color_fprintf(color_id, file, "%2d ", value);
          //  else fprintf(file, "%2d ", value);
        }
        j+=3;
        printf("\n");
    }
}

void fprint_matrix3D_graph(FILE *file, blc_mem *mem, int step, int length, int offset, int width, int height)
{
    uchar value;
    int color_id;
    int i, j;
    
    width=MIN(width/3, length*step);
    height=MIN(height, (int)(mem->size/step));
    
    FOR(j, height)
    {
        FOR(i, width)
        {
            value = (uchar)mem->data[j*length*step+i*step+offset]*100/256;
            if ( value < 20) color_id = BLC_BRIGHT_BLUE;
            else if ( value < 35 ) color_id = BLC_BLUE;
            else if ( value < 45 ) color_id = BLC_GREY;
            else if ( value < 55 ) color_id = BLC_WHITE;
            else if ( value < 65 ) color_id = BLC_BRIGHT_WHITE;
            else if ( value < 80 ) color_id = BLC_RED;
            else  color_id = BLC_BRIGHT_RED;
            color_fprintf(color_id, file,  "%2d ", value);
           /// else fprintf(file, "%2d ", value);
        }
        fprintf(file, "\n");
    }
}

void fprint_matrix_graph_hex(FILE *file, blc_mem *mem, int step, char const *title, int width, int height)
{
    uchar value;
    int color_id;
    int i, j;
    
    fprintf(file, "[%s]", title);
    fprintf_escape_command(file, DEL_END_LINE);
    fprintf(file, "\n");
    
    
    width=MIN(width/3, step/8);
    height=MIN(height-2, (int)(mem->size/(step*2)));
    
    FOR(j, height*4)
    {
        FOR(i, width)
        {
            value = (uchar)mem->data[j*step+i*8+1];
            if ( value < 48) color_id = BLC_BLUE;
            else if ( value < 96 ) color_id = BLC_BRIGHT_BLUE;
            else if ( value < 120 ) color_id = BLC_GREY;
            else if ( value < 136 ) color_id = BLC_WHITE;
            else if ( value < 160 ) color_id = BLC_BRIGHT_WHITE;
            else if ( value < 208 ) color_id = BLC_BRIGHT_RED;
            else  color_id = BLC_RED;
            color_fprintf(color_id, file,  "%2X ", value);
            ///else fprintf(file, "%2X ", value);
        }
        j+=3;
        fprintf(file, "\n");
    }
}

void fprint_matrix_graph_decimal(FILE *file, blc_mem *mem, int step, char const *title, int width, int height, int ansi_terminal)
{
    uchar value;
    int color_id;
    int i, j;
    
    fprintf(file, "[%s]", title);
    fprintf_escape_command(file, DEL_END_LINE);
    fprintf(file, "\n");
    
    if (ansi_terminal) width=MIN(width/3, step);
    else width=MIN(width/4, step);
    height=MIN(height-2, (int)(mem->size/step));
    
    FOR(j, height)
    {
        FOR(i, width)
        {
            value = (uchar)mem->data[j*step+i*8+1];
            if ( value < 50) color_id = BLC_BRIGHT_BLUE;
            else if ( value < 100 ) color_id = BLC_BLUE;
            else if ( value < 150 ) color_id = BLC_GREY;
            else if ( value < 200 ) color_id = BLC_BRIGHT_WHITE;
            else if ( value < 250 ) color_id = BLC_RED;
            else  color_id = BLC_BRIGHT_RED;
            if (ansi_terminal) color_fprintf(color_id, file, "%.2d ", value%100);
            else fprintf(file, "%3d ", value);
        }
        fprintf(file, "\n");
    }
}

void fprint_3Dmatrix(FILE *file, blc_mem *mem, int offset, int step0, int length0, int step1, int length1,  int step2,  int length2, int ansi_terminal)
{
    char *data;
    char tmp_string[8];
    int color_id, previous_color_id=-1;
    int i, j, k;
    uchar value;
    size_t console_size;
    
    if (ansi_terminal) 
    {
        console_size=length0*length1*length2*8+length2; //8 bytes per pixel for colors + return char for each line.
        if (!ansi_texts) {
            ansi_texts = MANY_ALLOCATIONS(256*2, char);
            uchar_bar_colors = MANY_ALLOCATIONS(256, char);
            FOR(i, 256)
            {
                sprintf(tmp_string, "%.2d ", i%100);
                memcpy(ansi_texts+i*2, tmp_string, 2);
                uchar_bar_colors[i] = blc_bar_colors[i*BLC_BAR_COLORS_NB/256];
            }
        }
    }
    else console_size = length0*length1*length2*4+length2; //4 bytes per pixel for colors + return char for each line.
    
    console_buffer.allocate_min(console_size);
    
    console_size=0;
    data = mem->data+offset;
    FOR(j, length2)
    {
        FOR(i, length1)
        {
            FOR(k, length0)
            {
                value = *data;
                if (ansi_terminal)
                {
                    color_id=uchar_bar_colors[value];
                    if (color_id != previous_color_id)
                    {
                        console_buffer.data[console_size++]=27; //ESC
                        console_buffer.data[console_size++]='[';
                        if (color_id & BLC_BRIGHT)
                        {
                            console_buffer.data[console_size++]='9';
                            console_buffer.data[console_size++]=color_id+40;
                        }
                        else
                        {
                            console_buffer.data[console_size++]='3';
                            console_buffer.data[console_size++]=color_id+48;

                        }
                        console_buffer.data[console_size++]='m';
                    }
                    console_buffer.data[console_size++]=ansi_texts[value*2];
                    console_buffer.data[console_size++]=ansi_texts[value*2+1];
       //             console_buffer.data[console_size++]=' ';
                    previous_color_id=color_id;
                }
                else fprintf(file, "%.3d ", value);
                data+=step0;
            }
            data+=step1 - step0*length0;
        }
        data+=step2 - step1*length1;
        if (j != length2-1) console_buffer.data[console_size++] = '\n';
        else if (!ansi_terminal) printf("\n\n"); //White line to separate the data
    }
    fwrite(console_buffer.data, console_size, 1, file);
    fprint_stop_color(stderr);
    fflush(file);
    
}
END_EXTERN_C
