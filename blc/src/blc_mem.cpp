/* Basic Library for C/C++ (blclib)
 Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (2011 - 2014)
 
 Author: A. Blanchard
 
 This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
 This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
 You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
  users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
  In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
  that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
 Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
  and, more generally, to use and operate it in the same conditions as regards security.
  The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms. */

//
//  Created by Arnaud Blanchard on 17/06/2014.
//
//

#include "blc_mem.h"
#include "blc.h"

blc_mem::blc_mem(size_t new_size)
: data(NULL), size(0)
{
    if (new_size != 0) allocate(new_size);
}

void blc_mem::allocate()
{
    if (data) EXIT_ON_ERROR("mem already allocated");
    else data = MANY_ALLOCATIONS(size, char);
}


void blc_mem::allocate(size_t new_size)
{
    if (new_size != size || data == NULL)  // Changement de taille ou l'allocation n'a encore jamais été faite
    {
        free(data);
        data = MANY_ALLOCATIONS(new_size, char);
        size = new_size;
    }
}

void blc_mem::allocate_min(size_t new_size)
{
    if (new_size > size)
    {
        free(data);
        data = MANY_ALLOCATIONS(new_size, char);
        size = new_size;
    }
}

void blc_mem::reallocate(size_t new_size)
{
    MANY_REALLOCATIONS(&data, new_size);
    size = new_size;
}

void blc_mem::replace(char const*new_data, size_t new_size)
{
    allocate(new_size);
    memcpy(data, new_data, new_size);
}

void blc_mem::append(char const*new_data, size_t new_size)
{
    size_t previous_size = size;
    reallocate(size + new_size);
    memcpy(&data[previous_size], new_data, new_size);
}

void blc_mem::reset(int value)
{
    memset(data, value, size);
}



/******  blc_narray ******/

blc_narray::blc_narray(int dims_nb):blc_mem(),lengths(NULL), dims_nb(dims_nb){
    
    steps=MANY_ALLOCATIONS(dims_nb, size_t);
    lengths=MANY_ALLOCATIONS(dims_nb, int);
}


blc_narray::blc_narray(int dims_nb, int length, ...):dims_nb(dims_nb){
    int i;
    va_list args;
    
    steps=MANY_ALLOCATIONS(dims_nb, size_t);
    lengths=MANY_ALLOCATIONS(dims_nb, int);
    
    data=NULL;
    size=1;
    va_start(args, length);
    FOR(i, dims_nb)
    {
        size*=length;
        steps[i]=1;
        lengths[i]=length;
        length=va_arg(args, int);
    }
    va_end(args);
}

void blc_narray::destroy(){
    free(lengths);
    free(steps);
}

void blc_narray::add_dim(int length, int step){
    APPEND_ITEM(&lengths, &dims_nb, &length);
    MANY_REALLOCATIONS(&steps, dims_nb);
    steps[dims_nb-1]=step;
    size=step*length;
}

void blc_narray::add_dim(int length){
    if (dims_nb==0) add_dim(length, 1);
    else add_dim(length, lengths[dims_nb-1]*steps[dims_nb-1]);
}


void blc_narray::set_dims(int dims_nb, int length, ...)
{
    int i;
    va_list args;
    
    FREE(steps);FREE(lengths);
    this->dims_nb=dims_nb;
    steps=MANY_ALLOCATIONS(dims_nb, size_t);
    lengths=MANY_ALLOCATIONS(dims_nb, int);
    
    data=NULL;
    size=1;
    va_start(args, length);
    FOR(i, dims_nb)
    {
        size*=length;
        steps[i]=1;
        lengths[i]=length;
        length=va_arg(args, int);
    }
    va_end(args);
}


int blc_narray::get_type_size()
{
    uint32_t type_print;
    switch (type){
        case'INT8': case 'UIN8': case 'NDEF': return 1;
        case 'UI16':case 'IN16':  return 2;
        case 'FL32':case 'UI32':case 'IN32': return 4;
        case 'FL64': case'UI64':case 'IN64': return 8;
        default: EXIT_ON_ERROR("The format '%.4s' is unknown.", UINT32_TO_STRING(type_print, type));
    }
    return -1;
}

int blc_narray::sprint_dims(char *string, int string_size)
{
    int i, width=0;
    
    width+=sprintf(string, "%d", lengths[0]);
    for(i=1; i<dims_nb; i++) width+=sprintf(string+width, "x%d", lengths[i]);
    if (width >= string_size) EXIT_ON_ERROR("The reserved size %d is too small to store the %d dims.",size, dims_nb);
    return width;
}


int blc_narray::fprint_dims(FILE *file)
{
    int i, width=0;
    
    width+=fprintf(file, "%d", lengths[0]);
    for(i=1; i<dims_nb; i++) width+=fprintf(file, "x%d", lengths[i]);
    return width;
}

size_t blc_narray::get_minimum_size()
{
    int i;
    size_t size = get_type_size();
    
    FOR_INV(i, dims_nb) {
        if (lengths[i]==0) EXIT_ON_ERROR("Length must not be 0 on dim '%d'");
        size*=lengths[i];
    }
    return size;
}


/* C wrapper */
extern "C" {
    void blc_mem_allocate(blc_mem *mem, size_t size)
    {
        mem->allocate(size);
    }
    
    void blc_mem_reallocate(blc_mem *mem, size_t new_size)
    {
        mem->reallocate(new_size);
    }
    
    void blc_mem_replace(blc_mem *mem, const char *data, size_t size)
    {
        mem->replace(data, size);
    }
    
    void blc_mem_append(blc_mem *mem, const char *data, size_t size)
    {
        mem->append(data, size);
    }
    
    void blc_narray_destroy(blc_narray *narray)
    {
        FREE(narray->data);
        FREE(narray->lengths);
        FREE(narray->steps);
    }
    
}

