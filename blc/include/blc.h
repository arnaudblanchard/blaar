/* Basic Library for C/C++ (blclib)
 Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (2011 - 2015)

 Author: A. Blanchard

 This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
 You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
 users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
 In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
 Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
 and, more generally, to use and operate it in the same conditions as regards security.
 The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms. */

#ifndef BLC_H
#define BLC_H
/**
@mainpage
@author Arnaud Blanchard
@date 2011 - 2015
@copyright ETIS - ENSEA, University of Cergy-Pontoise, CNRS (2011-2015) - CeCILL v2.1
 
 This library is a set of basic tools simplifying the programatio. The programmer keeps full control on the program. You have shortcuts for classical functionnalities and very little abstractions. You can freely mix blc and low level C/C++ functions. The modules are minimalist in purpose. If you want more your are adviced to use specialized libraries. The goal is to make it works on **POSIX** systems. **GNU/Linux** (Ubuntu), **Darwin** (Mac OSX) and **bionic** (Android).
 
@section Installation

Use the traditional way to install blaa projects ( [../README.md] ).
 
Alternativly you may use your own command lines inspired from:
@code{.sh}
g++ -c -Iinclude src"/"*.cpp
ar crs *.o libblc.a
@endcode
@section Modules
 
@subpage blc_text

@subpage blc_tools
 
@subpage blc_mem

@subpage blc_realtime
 
@subpage blc_network

@subpage blc_channel
 
@subpage blc_text_graph
 
@subpage blc_text_app
 */

#include "blc_text.h"
#include "blc_tools.h"
#include "blc_mem.h"
#include "blc_realtime.h"
#include "blc_network.h"
#include "blc_channel.h"
#include "blc_text_graph.h"
#include "blc_text_app.h"
#include "blc_image.h"
#include "iniparser.h"
#endif
