Basic Library for C/C++
=======================

Copyright : [ETIS](http://www.etis.ensea.fr/neurocyber) - ENSEA, University of Cergy-Pontoise, CNRS (2011-2016)  
Author    : [Arnaud Blanchard](http://arnaudblanchard.thoughtsheet.com)  
Licence   : [CeCILL v2.1](http://www.cecill.info/licences/Licence_CeCILL_V2-en.html)

See examples in examples/


