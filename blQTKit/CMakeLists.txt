cmake_minimum_required(VERSION 2.6)

# Set the name and the supported language of the project
# Set the name of the project as the directory basename
get_filename_component(BASE_NAME ${CMAKE_SOURCE_DIR} NAME)
project(${BASE_NAME})

add_definitions(-Wextra -Wall) # -march=generic -mtune=switch)

find_library(QUARTZ_CORE_LIBRARY QuartzCore)
find_library(APPKIT_LIBRARY AppKit)
find_library(QTKIT_LIBRARY QTKit)

if (NOT TARGET shared_blc)
add_subdirectory(${BLAAR_DIR}/blc blc)  
endif()
	
add_library(static_blQTKit STATIC src/blQTKit.mm)
add_library(shared_blQTKit SHARED src/blQTKit.mm)

set_target_properties(static_blQTKit PROPERTIES OUTPUT_NAME blQTKit)
set_target_properties(shared_blQTKit PROPERTIES OUTPUT_NAME blQTKit)

include_directories(include ${BLAAR_DIR}/blc/include)

target_link_libraries(shared_blQTKit ${QTKIT_LIBRARY} ${APPKIT_LIBRARY} ${QUARTZ_CORE_LIBRARY} shared_blc)
