/*
 *  blQTKit.h
 *  blQTKitlib
 *
 *  Created by Arnaud Blanchard on 31/01/15.
 *  Copyright 2015 ETIS. All rights reserved.
 *
 */


#ifndef BLQTKIT_H
#define BLQTKIT_H

#include "blc_tools.h"
#include <QuartzCore/QuartzCore.h>

START_EXTERN_C
void init_capture(void (*callback)(CVImageBufferRef, void*), void *user_data);
END_EXTERN_C

#endif