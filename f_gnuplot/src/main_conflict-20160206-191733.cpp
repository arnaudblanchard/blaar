#include "blc.h"
#include <unistd.h>
#include <signal.h>
#include <armadillo>
#include <error.h>

blc_channel *channel;
char command[LINE_MAX + 1];

float sx, sy;
FILE *pipef, *bin_pipe;
int refresh_period, interactive_mode;
int final_width, final_height, width, height;
int offset = 0;
int bands = 1;
int dims_nb, *lengths;
double min, max;
enum {
   RUN, PAUSE
};
int status = RUN;

static void quit()
{
   printf("Quitting %s\n", blc_program_name);
   exit(0);
}

static void plot_uchar()
{
   int i, j, letter;
   arma::fvec vec(sx * sy);
   arma::uchar_cube cube((uchar*) channel->data, bands, width, height, false, true);
   arma::uchar_mat umat(final_width, final_height);

   while (1)
   {
      if (interactive_mode)
      {
         letter = getc(stdin);
         switch (letter)
         {
         case -1:
         case 'q':
            quit();
            break;
         case '.':
            break;
         default:
            ;
         }
      }
      else blc_command_try_to_interpret();

      fprintf(pipef, "%s\n", command);
      FOR(i, final_width)
         FOR(j, final_height)
         {
            vec = arma::conv_to<arma::fvec>::from((arma::uchar_vec) cube(offset, i * sx, j * sy, arma::size(1, sx, sy)));
            umat(i, j) = mean(vec);
         }
      fwrite(umat.memptr(), 1, umat.n_elem, bin_pipe);
      fflush(bin_pipe);
      if (interactive_mode == 0) usleep(refresh_period * 1000);
   }
}

static void plot_float()
{
   int i, j, letter;
   arma::fvec vec(sx * sy);
   arma::fcube cube((float*) channel->data, bands, width, height, false, true);
   arma::fmat fmat(final_width, final_height);
   while (1)
   {
      if (interactive_mode)
      {
         letter = getc(stdin);
         switch (letter)
         {
         case -1:
         case 'q':
            quit();
            break;
         case '.':
            break;
         default:
            ;
         }
      }
      else blc_command_try_to_interpret();

      fprintf(pipef, "%s\n", command);
      FOR(i, final_width)
         FOR(j, final_height)
         {
            vec = arma::conv_to<arma::fvec>::from((arma::fmat) cube(0, i * sx, j * sy, arma::size(1, sx, sy)));
            fmat(i, j) = mean(vec);
         }
      fwrite(fmat.memptr(), 1, final_width * final_height * sizeof(float), bin_pipe);
      fflush(bin_pipe);
      if (interactive_mode == 0) usleep(refresh_period * 1000);
   }
}

static void create_graph()
{
   uint32_t print_uint32;

   switch (channel->dims_nb)
   {
   case 1:
      switch (channel->format)
      {
      case 'NDEF':
         width = channel->lengths[0];
         height = 1;
         break;
      default:
         EXIT_ON_ERROR("The format '%.4s' is not managed for number of dims %d", UINT32_TO_STRING(print_uint32, channel->format), channel->dims_nb);
      }
      break;

   case 2:
      switch (channel->format)
      {
      case 'NDEF':
      case 'Y800':
         width = channel->lengths[0];
         height = channel->lengths[1];
         break;
      default:
         EXIT_ON_ERROR("The format '%.4s' is not managed", UINT32_TO_STRING(print_uint32, channel->format));
      }
      break;
   default:
      EXIT_ON_ERROR("The number of dims '%d' is not yet managed", channel->dims_nb);
   }

   final_width = MIN(final_width, width);
   final_height = MIN(final_height, height);

   sx = width / final_width;
   sy = height / final_height;

   SYSTEM_ERROR_CHECK(pipef = popen("gnuplot", "w"), NULL, "Launching gnuplot");
 //  SYSTEM_ERROR_CHECK(bin_pipe = freopen(NULL, "wb", pipef), 0,  "Reopening pipe gnuplot");
   SYSTEM_ERROR_CHECK(bin_pipe = fdopen(fileno(pipef), "wb"), 0,  "Reopening pipe gnuplot");

   printf("Ok2\n");


   fprintf(pipef, "set term qt\n");
   fprintf(pipef, "set term qt 1 noraise\n");
   fprintf(pipef, "set view 30,190\n");
   fprintf(pipef, "set zrange [%f:%f]\n", min, max);

   // fprintf(pipe, "set palette grey\n");
   //   fprintf(pipe, "set pm3d at ts\n");

   switch (channel->type)
   {
   case 'UIN8':
      SPRINTF(command, "splot '-' binary format='%%uchar' array=%dx%d title '%s' with pm3d", final_width, final_height, channel->name + 1);
      plot_uchar();
      break;
   case 'INT8':
      SPRINTF(command, "splot '-' binary format='%%char' array=%dx%d with pm3d", final_width, final_height);
      break;
   case 'FL32':
      SPRINTF(command, "splot '-' binary format='%%float' array=%dx%d title '%s' with pm3d", final_width, final_height, channel->name + 1);
      plot_float();
      break;
   default:
      EXIT_ON_ERROR("The type '%.4s' is not managed", UINT32_TO_STRING(print_uint32, channel->type));
   }
}
void pause_cb()
{
   printf("pause\n");
   getc(stdin);
   printf("continue\n");
}

void refresh_period_cb(char const*, char const*argument, void*)
{
   refresh_period = atof(argument);
}

int main(int argc, char *argv[])
{

   char channel_name[PATH_MAX + 1];
   int status = 0;
   uint32_t print_type;

   char const *help, *refresh_string, *min_str, *max_str, *size_str;

   // terminal_ansi_detect();
   program_set_description("Display the content of the blc_channel depending on its type and format");
   program_add_parameter("blc_channel", 0, "channel name you want to display");
   program_option_add(&help, 'h', "help", NULL, "display this help.", NULL);
   program_option_add(&min_str, 'm', "min", "FL32", "minimal value", "0");
   program_option_add(&max_str, 'M', "max", "FL32", "maximal value", NULL);
   program_option_add(&refresh_string, 'r', "refresh", "UI32", "refresh period in ms", "1000");
   program_option_add(&size_str, 's', "size", "UI32[xUI32]", "size of the mask", "32x32");

   program_option_interpret(&argc, &argv);

   if (help) program_option_display_help();
   else
   {
      if (argc == 1)
      {
//            SYSTEM_ERROR_CHECK(channel_name_length=getline(&channel_name, &channel_name_cap, stdin),-1, NULL); // should be freed
//            channel_name[channel_name_length-1]=0;
         scanf("%s\n", channel_name);
         interactive_mode = 1;
      }
      else if (argc == 2)
      {
         strcpy(channel_name, argv[1]);
      }
      else
      {
         program_option_display_help();
         EXIT_ON_ERROR("You need a blc_channel as first argument.");
      }
      dims_nb = blc_sscan_dims(&lengths, size_str);
      switch (dims_nb)
      {
      case 1:
         final_width = lengths[0];
         final_height = 1;
         break;
      case 2:
         final_width = lengths[0];
         final_height = lengths[1];
         break;
      default:
         EXIT_ON_ERROR("Size of dim %d is not yet managed in size '%s'.", dims_nb, size_str);
      }

      refresh_period = atoi(refresh_string); //deprecated should be strtod

      channel = new blc_channel(channel_name);

      if (min_str) min = atof(min_str);
      else switch (channel->type)
      {
      case 'UIN8':
      case 'UI16':
      case 'UI32':
      case 'IN16':
      case 'IN32':
      case 'IN64':
      case 'FL32':
      case 'FL64':
         min = 0.0;
         break;
      case 'INT8':
         min = INT8_MIN;
         break;
      default:
         EXIT_ON_ERROR("No default min value for type '%.4s'", UINT32_TO_STRING(print_type, channel->type));
      }

      if (max_str) max = atof(max_str);
      else switch (channel->type)
      {
      case 'UIN8':
         max = UINT8_MAX;
         break;
      case 'INT8':
         max = INT8_MAX;
         break;
      case 'FL32':
      case 'FL64':
         max = 1.0;
         break;
      default:
         EXIT_ON_ERROR("No default max value for type '%.4s'", UINT32_TO_STRING(print_type, channel->type));
      }

      blc_command_add("", (type_blc_command_cb) pause_cb, NULL, "pause", NULL);
      blc_command_add("period", refresh_period_cb, "time(ms)", "refresh period", NULL);
      blc_command_add("q", (type_blc_command_cb) quit, NULL, "quit", NULL);

      create_graph();
   }
   return (status);
}

