#ifndef CORE_HPP
#define CORE_HPP

#include <gtk/gtk.h>

extern GtkWidget *window;


int blgtk_toggled(GtkToggleToolButton *toggle_button);
GtkWidget *blgtk_add_tool_button(GtkWidget *toolbar, gchar const *label, gchar const *icon_name, GCallback callback, gpointer user_data);
GtkToggleToolButton *blgtk_add_toggle_tool_button(GtkWidget *toolbar, gchar const *label, gchar const *icon_name, GCallback callback, gpointer user_data);

#endif