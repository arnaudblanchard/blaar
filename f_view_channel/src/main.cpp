#include "main.hpp"

#include <fcntl.h> // O_RDONLY ...
#include <stdio.h>
#include <gtk/gtk.h>
#include <string.h>
#include <stdint.h> //uint32_t
//#include <jpeglib.h>
#include <sys/mman.h>
#include <errno.h> //errno
#include "blc.h"

#include "byte_display.hpp"
#include "float_display.hpp"

const char *channel_name;
GtkWidget *window;
GdkDisplay *main_display;
GdkDevice *pointer_device;
int interactive_mode=0;
GtkApplication *app;
blc_channel mouse_channel;

blc_channel input;
int input_terminal;

char const *format_string=NULL;
uint32_t format;

void activate_cb(GApplication *app)
{
    GtkWidget *display=NULL;
    
    main_display = gdk_display_get_default ();
    GdkDeviceManager *device_manager = gdk_display_get_device_manager (main_display);
    pointer_device = gdk_device_manager_get_client_pointer (device_manager);
    
    uint32_t type_string;
    char title[NAME_MAX+1];
    
    window=gtk_application_window_new(GTK_APPLICATION(app));
    SPRINTF(title, "%s %s", blc_program_name, input.name);
    gtk_window_set_title(GTK_WINDOW(window), title);
    switch (input.type)
    {
        case 'UIN8': display=create_byte_display(&input);
            break;
        case 'UI32':case 'FL32': display=create_float_display(&input);
            break;
        default:
            type_string=ntohl(input.type);
            EXIT_ON_ERROR("Type '%.4s' not managed.", &type_string);
    }
    
    gtk_container_add(GTK_CONTAINER(window), display);
    gtk_widget_show_all(window);
}

void on_quit()
{
    if (mouse_channel.fd==-1) mouse_channel.unlink();
}

/** Classical GTK application.
 * The first optional argument is the name of the experience. Otherwise all the existing shared memory are used.
 * */
int main(int argc, char *argv[])
{
    int status=0;
    char const  *g_debug;
    blc_channel channel_info;
    
    blc_program_set_description("Display the content of the blc_channel depending on its type on format");
    blc_program_add_channel_parameter(&input, 1, "channel name you want to display");
    blc_program_option_add(&format_string, 'f', "format", "TBOX|VMET|CBOX", "floats in text boxes, viewmeters or check boxes", "NDEF");
    blc_program_add_channel_option(&mouse_channel, 'm', "mouse", "return the mouse coordinates in the channel", NULL);
    blc_program_option_add(&g_debug, ' ', "g-fatal-warnings", NULL, "Debug gtk.", NULL);
    blc_program_init(&argc, &argv, on_quit);
    
    input.open();
    format=STRING_TO_UINT32(format_string);
        
  //  if (mouse_channel_name) mouse_channel=new blc_channel(mouse_channel_name, 3, BLC_SEM_PROTECT, 'UI32', 'TEXT', NULL);

    blc_command_add("q", (type_blc_command_cb) exit, NULL, "quit", NULL);
    blc_command_interpret_thread("h");
    
    gtk_disable_setlocale();
    gtk_init(&argc, &argv);
    app = gtk_application_new(NULL, G_APPLICATION_FLAGS_NONE);
    g_signal_connect (app, "activate", G_CALLBACK (activate_cb), NULL);
    status = g_application_run(G_APPLICATION(app), 0, NULL);
    g_object_unref(app);
    
    return (status);
}

