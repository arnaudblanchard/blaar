#ifndef FLOAT_DISPLAY_HPP
#define FLOAT_DISPLAY_HPP
#include <gtk/gtk.h>
#include "blc_channel.h"

GtkWidget *create_float_display(blc_channel *channel);


#endif