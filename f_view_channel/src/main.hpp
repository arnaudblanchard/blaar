#ifndef MAIN_HPP
#define MAIN_HPP

#include <gtk/gtk.h>
#include <blc_channel.h>

extern GtkWidget *window;
extern GdkDisplay *main_display;
extern GdkDevice *pointer_device;
extern GtkApplication *app;
extern blc_channel mouse_channel;

extern int interactive_mode;
extern uint32_t format;

void quit();
#endif