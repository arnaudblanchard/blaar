#!/bin/sh

./compile.sh $1 "debug" $1 || exit 1


bin_dir="${PWD}_build/debug_bin"

program=`basename $1`
shift      #remove $1
echo
echo "execute: $bin_dir/$program $*"

command -v valgrind && valgrind  --dsymutil=yes $bin_dir/$program $* || echo "You need to install valgrind"



