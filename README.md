[[DEVELOPMENT]] | [[CMAKE]]

Basic Libraries And Applications for Robotics (blaar)
====================================================

Copyright : [ETIS](http://www.etis.ensea.fr/neurocyber) - ENSEA, University of Cergy-Pontoise, CNRS (2011-2016)  
Author    : [Arnaud Blanchard](http://arnaudblanchard.thoughtsheet.com)  
Licence   : [CeCILL v2.1](http://www.cecill.info/licences/Licence_CeCILL_V2-en.html)

Basic libraries
---------------

- [blc](blc/README) : Basic Library for C/C++. Very generic tools
- blv4l2 : Basic Library for video capture on Linux. Need video for linux libv4l2.
- blQTKit : Basic Library for video on MAC. Need Quicktime libQTKit.
- ...

Usage
=====

    ./run.sh <project> [args] ...

This automatically compiles the project in release mode and executes it. It uses `rlwrap` to have much better keyboard interaction such as reediting and using history but may sometimes confuse the terminal.

For more details about compilation, see [[DEVELOPMENT]].



Need to install packages
========================

OSX
---
    brew install git cmake doxygen pkgconfig gtk+3 vte3 gnuplot gnome-icon-theme

Ubuntu
------
    sudo apt-get install git cmake doxygen pkgconfig gtk+3 nvidia-opencl-dev
    
