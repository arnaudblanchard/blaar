//
//  Created by Arnaud Blanchard on 06/11/14.
//  Copyright ETIS 2014. All rights reserved.
//

#include "blc.h"
#include <string.h>

#include "blv4l2.h"
#include <limits.h>


enum status {STOP, PAUSE, RUN};

blc_channel output;
int status=RUN;

void quit_callback()
{
   printf("q");
   fflush(stdout);
}

void callback(blc_narray *image, void *)
{
   blc_program_loop_start();
	output.protect();
	blc_image_copy(&output, image);
	memcpy(output.data, image_mem->data, output.size);
	output.protect(0);
   printf(".");
   fflush(stdout);

	if (status == STOP) exit(0);
   blc_program_loop_end();

}

int main(int argc, char **argv)
{
   char const *device_name;
	blv4l2_device camera;
	int bytes_per_pixel;

   blc_program_set_description("Acquire webcam images on Linux computer (with video4linux2).");
   blc_program_option_add(&device_name, 'd', "device", "path", "device path name", "/dev/video0");
   blc_program_add_channel_option(&output, 'o', "output", "output channel name", "/image");
   blc_program_init(&argc, &argv, quit_callback);

   camera.init(device_name);

	output.type='UIN8';
   output.format=ntohl(camera.format.fmt.pix.pixelformat);
   bytes_per_pixel=blc_get_bytes_per_pixel(&output);
   switch (bytes_per_pixel)
   {
      // Variable size (i.e. compressed image like JPEG)
      case -1:output.add_dim(camera.format.fmt.pix.sizeimage/(camera.format.fmt.pix.width*camera.format.fmt.pix.height));
      break;
      case 1:break; //no need for a dimension on the pixels size
      default:output.add_dim(bytes_per_pixel);
   }
   output.add_dim(camera.format.fmt.pix.width);
   output.add_dim(camera.format.fmt.pix.height);

   output.create_or_open(output.name, BLC_SEM_PROTECT, NULL);

   printf("%s\n", output.name);
   fflush(stdout);

   blc_program_loop_init();
	camera.start(callback, NULL);
	
	return 0;
}
