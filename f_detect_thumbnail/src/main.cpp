//
//  Created by Arnaud Blanchard on 06/11/14.
//  Copyright ETIS 2014. All rights reserved.
//

#include "main.h"
#include <arrayfire.h>

#include "blc.h"
#include "blcl.h"
#include <sys/signal.h>
#include <stdlib.h>
#include <unistd.h>

enum {RUN, PAUSE, STOP, STATUS_NB};
struct blcl_target gpu_target;
size_t work_group_sizes[2];
size_t items_nbs[2];
//uint32_t output_format;

/**
 @main
 Exemple of using the console to display the camera
 */
int status=RUN;
double period;
int iteration=0;
struct timeval timestamp={};
char const *force_ansi=NULL;
blc_narray input;
int scale;
char  const *channel_name;
blc_channel *filter=NULL;
blc_channel output;
int pipe_mode;

static int initialised = 0;

static uint32_t input_format;
struct timeval global_time;
size_t image_size=0;

char const *Y0_code="y0";
char const *U_code="u";
char const *Y1_code="y1";
char const *V_code="v";
char *source_code=NULL;

int refresh_filter=1;
cl_mem cl_input, cl_output, cl_mask;
cl_kernel kernel;

char *activation=(char*)"convert_uchar_sat(p)";

// Simple compute kernel which computes the square of an input array
const char *start_source_code = \

"kernel void filter(global uchar output[OUTPUT_HEIGHT][OUTPUT_WIDTH], global uchar2 input[INPUT_HEIGHT][INPUT_WIDTH], constant float filter[FILTER_HEIGHT][FILTER_WIDTH]){\n" \
"   int i,j;\n"\
"   uchar y0, u, y1, v;\n"\
"   uchar Y0, U, Y1, V;\n"\
"   uchar y01, u1, y11, v1;\n"\
"   local uchar tr[512]; \n"\
"   uchar tmp0, tmp1, tmp2, tmp3;\n"\
"   size_t x = get_global_id(0);\n"\
"   size_t y= get_global_id(1);\n"\
"   for(i=FILTER_WIDTH,i--;)\n"\
"   for(j=FILTER_HEIGHT,j--;){\n"\
"        output[i][j]=input[i][j].y;\n"\
"    }\n"\
"}\n";

char const* end_source_code="output[i+1] = Y0; output[i] = U; output[i+3] = Y1; output[i+2] = V;\n" \
"   }\n" \
"}\n";


cl_kernel update_kernel(blcl_target *target, blc_channel *output, blc_narray *input, blc_channel *filter)
{
    char  *defines, *source_code;
    cl_int error_id;
    cl_kernel kernel;

    
    asprintf(&defines,
             "#define FILTER_WIDTH %d\n#define FILTER_HEIGHT %d\n"\
             "#define INPUT_WIDTH %d\n#define INPUT_HEIGHT %d\n"\
             "#define OUTPUT_WIDTH %d#define OUTPUT_HEIGHT %d\n", filter->lengths[0],  filter->lengths[1], input->lengths[0], input->lengths[1], output->lengths[0], output->lengths[1]);
    asprintf(&source_code,\
             "%s\n"\
             "kernel void filter(global uchar output[OUTPUT_HEIGHT][OUTPUT_WIDTH], global uchar2 input[INPUT_HEIGHT][INPUT_WIDTH], constant float filter[FILTER_HEIGHT][FILTER_WIDTH]){\n" \
             "   int i,j;\n"\
             "   uchar y0, u, y1, v;\n"\
             "   uchar Y0, U, Y1, V;\n"\
             "   uchar y01, u1, y11, v1;\n"\
             "   local uchar tr[512]; \n"\
             "   uchar tmp0, tmp1, tmp2, tmp3;\n"\
             "   size_t x = get_global_id(0);\n"\
             "   size_t y= get_global_id(1);\n"\
             "   for(i=FILTER_WIDTH,i--;)\n"\
             "   for(j=FILTER_HEIGHT,j--;){\n"\
             "        output[i][j]=input[i][j].y;\n"\
             "    }\n"\
             "}\n", defines);
    
 
    BLCL_ERROR_CHECK(cl_input=clCreateBuffer(target->context, CL_MEM_READ_ONLY, input->size, NULL, &error_id), NULL, error_id,  NULL);
    BLCL_ERROR_CHECK(cl_mask=clCreateBuffer(target->context, CL_MEM_READ_ONLY, filter->size, NULL, &error_id), NULL, error_id,  NULL);
    BLCL_CHECK(clEnqueueWriteBuffer(target->command_queue, cl_mask, CL_TRUE, 0, filter->size, filter->data, 0, NULL, NULL ), "Failed to write input array!");
    kernel=BLCL_CREATE_KERNEL_2P(target, "filter", source_code, cl_input, cl_mask);

    free(defines);
    free(source_code);
    return kernel;
}

static void command_callback(char const *command, char const *, void *)
{
    uint32_t input_format_string, output_format_string;
    
    switch (*command)
    {
        case   0:if (status==RUN){
            status=PAUSE;
            fprintf(stderr, "%d iterations, average period: %.3lf ms\n", iteration, period/1000);
            iteration=0;
            blc_command_display_help();
            fprintf(stderr, "--- pause ---\n");
            blc_command_interpret();
        } else {
            status=RUN;
            fprintf(stderr, "--- continue ---\n");
        }
            break;
        case 'i':
            input_format_string=htonl(input.format);
            output_format_string=htonl(output.format);
            fprintf(stderr, "input:%.4s output:%.4s\n", (char*)&input_format_string, (char*)&output_format_string);
            printf("i");
            fflush(stdout);break;
        case 'q': status=STOP;break;
    }
}


static void edit_callback(char const *, char const *argument, void *user_data)
{
    struct timeval time={0, 0};
    if (strlen(argument)==0)  fprintf(stderr, "=%s\n", *((char**)user_data));
    else {
        //    free(user_data);
        *((char**)user_data)=strdup(argument);
        us_time_diff(&time);
        //  BLCL_CREATE_KERNEL_4P("square", update_source_code(), cl_output, cl_inputs, size, frame);
        fprintf(stderr, "Recompilation time %ld µs\n", us_time_diff(&time));
    }
}

static void quit()
{
    printf("q");
    fflush(stdout);
    output.unlink();
    blc_quit();
    blcl_release(&gpu_target);
}

void update_activation_cb(char const *, char const *argument, void *)
{
    if (strlen(argument)==0) fprintf(stderr, "%s\n", activation);
    else {
        activation=(char*)argument;
        refresh_filter=1;
    }
}

int main(int argc, char **argv)
{
    char const *help, *output_format_option, *filter_channel_name;
    
    program_option_add(&force_ansi, 'a', "ansi", 0, "force ansi mode", NULL);
    program_option_add(&output_format_option, 'f', "format", "Y800", "set ouput video format", "NDEF");
    program_option_add(&filter_channel_name, 'F', "filter", "blc_channel", "filter to convolute with", NULL);
    program_option_add(&help, 'h', "help", 0, "display help", NULL);
    program_option_add(&channel_name, 'o', "output", "blc_channel", "output channel name", NULL);
    // program_option_add(&scale_option, 's', "scale", "value", "dividing scale", );
    program_option_interpret(&argc, &argv);
    
    if (help) program_option_display_help();
    else{
        output.format = STRING_TO_UINT32(output_format_option);
        if (channel_name==NULL)
        {
            asprintf((char**)&channel_name, "/image%d", getpid());
            fprintf(stdout, "%s\n", channel_name);
        }
        pipe_mode=!(isatty(STDOUT_FILENO));
        
        atexit(quit);
        
        blc_command_add("", command_callback, NULL, "pause", NULL);
        blc_command_add("activation=", update_activation_cb, "expression", "change the activation function", NULL);
        blc_command_add("i", command_callback, NULL, "info", NULL);
        blc_command_add("q", command_callback, NULL, "quit", NULL);
        
        blcl_init(&gpu_target, CL_DEVICE_TYPE_CPU);
        
        if (filter_channel_name) filter=new blc_channel(filter_channel_name);
        

        
        
        return 0;
    }
}
