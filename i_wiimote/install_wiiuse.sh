#!/bin/bash

cd wiiuse-master
mkdir build
cd build
cmake-gui ..
make

cp src/libwiiuse.so ~/.local/lib

cd ../..
gcc echange.cpp -o moncode -I/home/neuro/.local/include -L/home/neuro/.local/lib -lblc -lrt -lwiiuse
