#!/bin/sh

if [ ! $1 ];
then
echo "You need to precise the project directory to compile.";
echo "usage: compile.sh <project> [ release | debug | relwithdebuginfo | minsizrel ]   [<target>]"
exit 1;
fi

project_dir="$PWD/$1"
current_dir="$PWD"
blaar_build_dir="${PWD}_build"

if [ $2 ];then
build_type="$2"
else
build_type="release"
fi

build_dir="$blaar_build_dir/${build_type}_build/$1"

mkdir -p ${blaar_build_dir}/doc/$1
mkdir -p $build_dir && cd $build_dir

echo "cmake build dir: $PWD"

[ -f Makefile ] || cmake $project_dir -DCMAKE_BUILD_TYPE=$build_type
make ${3%/} #remove the eventual last '/'
result=$?
cd $current_dir
exit $result

