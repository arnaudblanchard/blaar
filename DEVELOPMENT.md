Compilation 
===========

Use `./compile.sh <project dir> [release|debug]` (blc, blc_examples, blv4l2, ...)

The binaries will be in `../blaar_buid/<release_bin|debug_bin>/` and the libraries in `../blaar_buid/<delease_lib|debug_lib>/`

In case of problem you may debug: `./debug.sh <project>` or realize memory check with valgrind: `./valgrind.sh <project>`

The cmake build will be in  `../blaar_buid/<project>/`. ( more details about [[CMAKE]] )

Using an IDE
------------

If you want to edit the code, you can use:

* [With eclipse](ECLIPSE)
* [With xcode](XCODE)

Code documentation
==================

To build and see the documentatoin do: `./doc.sh <project>`

The built documentation will be in `../blaar_build/doc/<project>/html/index.html` but it will be launched automatically.

[[blgtk/GTK]]
