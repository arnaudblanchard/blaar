//
//  Created by Arnaud Blanchard on 06/11/14.
//  Copyright ETIS 2014. All rights reserved.
//

#include "main.h"
#include "blc.h"
#include "blcl.h"
#include "blQTKit.h"
#include <sys/signal.h>
#include <stdlib.h>
#include <unistd.h>

enum {RUN, PAUSE, STOP, STATUS_NB};
struct blcl_target gpu_target;
size_t work_group_sizes[2];
size_t items_nbs[2];
//uint32_t output_format;


/**
 @main
 Exemple of using the console to display the camera
 */
double period;
int iteration=0;
struct timeval timestamp={};
char const *force_ansi=NULL;
int scale;
char  const *channel_name;
blc_channel input, output;
int output_terminal, input_terminal;

struct timeval global_time;
size_t image_size=0;

char const *Y0_code="y0";
char const *U_code="u";
char const *Y1_code="y1";
char const *V_code="v";
char *source_code=NULL;

int refresh_filter=1;
cl_mem cl_input, cl_output, cl_mask;
cl_kernel kernel;

int status=RUN;


char *activation=(char*)"convert_uchar_sat(p)";

// Simple compute kernel which computes the square of an input array

cl_kernel update_kernel(blcl_target *target, blc_channel *output, blc_narray *input)
{
    char  *defines, *source_code;
    cl_int error_id;
    cl_kernel kernel;
    
    asprintf(&defines,
             "#define TILE_WIDTH 34\n#define TILE_HEIGHT 18\n"\
             "#define INPUT_WIDTH %d\n#define INPUT_HEIGHT %d\n"\
             "#define OUTPUT_WIDTH %d\n#define OUTPUT_HEIGHT %d\n", input->lengths[0], input->lengths[1], output->lengths[0], output->lengths[1]);
    /*  asprintf(&source_code,\
     "%s\n"\
     "kernel void filter(global uchar *output, global uchar *input){\n" \
     "   local uchar mem[TILE_WIDTH][TILE_HEIGHT];\n"\
     "   local char dx[TILE_WIDTH-1][TILE_HEIGHT-2];\n"\
     //  , dxn[TILE_WIDTH-1][TILE_HEIGHT-2];\n"
     "   local char dy[TILE_WIDTH-2][TILE_HEIGHT-1];\n"\
     "   local uchar ON[TILE_WIDTH-2][TILE_HEIGHT-2];\n"\
     "   local uchar OFF[TILE_WIDTH-2][TILE_HEIGHT-2];\n"\
     "   size_t x = get_global_id(0)+1;\n"\
     "   size_t y= get_global_id(1)+1;\n"\
     "   size_t k=get_local_id(0)+1;\n"\
     "   size_t l=get_local_id(1)+1;\n"\
     "   int val\n;"\
     "   mem[k][l]=input[x+y*INPUT_WIDTH];\n"\
     "   if (k==1){\n"\
     "     mem[0][l]=input[x-1+y*INPUT_WIDTH]; \n"\
     "     if (l==1) mem[k-1][0]=input[x-1+(y-1)*INPUT_WIDTH]; \n"\
     "     else if (l==TILE_HEIGHT-2) mem[k-1][l+1]=input[x-1+(y+1)*INPUT_WIDTH]; \n"\
     "   }else if (k==TILE_WIDTH-2){\n"\
     "     mem[k+1][l]=input[x+1+y*INPUT_WIDTH]; \n"\
     "     if (l==1) mem[k+1][0]=input[x+1+(y-1)*INPUT_WIDTH]; \n"\
     "     else if (l==TILE_HEIGHT-2) mem[k+1][l+1]=input[x+1+(y+1)*INPUT_WIDTH]; \n"\
     "   }\n"\
     "   if (l==1) mem[k][0]=input[x+(y-1)*INPUT_WIDTH]; \n"\
     "   else if (l==TILE_HEIGHT-2) mem[k][l+1]=input[x+(y+1)*INPUT_WIDTH]; \n"\
     "   barrier(CLK_LOCAL_MEM_FENCE);\n"\
     "   dx[k][l-1]=convert_char_sat(mem[k][l]-mem[k+1][l]);\n"\
     "   if (k==1) dx[0][l-1]=convert_char_sat(mem[0][l]-mem[1][l]);\n"\
     "   dy[k-1][l]=convert_char_sat(mem[k][l]-mem[k][l+1]);\n"\
     "   if (l==1) dy[k-1][0]=convert_char_sat(mem[k][0]-mem[k][1]);\n"\
     "   barrier(CLK_LOCAL_MEM_FENCE);\n"\
     " //  ON[k-1][l-1]=convert_uchar_sat(dx[k-1][l-1]+[l-1]+dyp[k-1][l-1]+dyn[k-1][l]+128);\n"\
     " //  OFF[k-1][l-1]=convert_uchar_sat(dxp[k+1][l]+dxn[k][l]+dyp[k][l+1]+dyn[k][l]);\n"\
     " //  output[(x-1)+(y-1)*OUTPUT_WIDTH]=convert_uchar_sat(ON[k-1][l-1]+OFF[k-1][l-1]);\n"\
     "     output[(x-1)+(y-1)*OUTPUT_WIDTH]=abs(dy[k-1][l-1])+abs(dx[k-1][l-1]);\n"\
     " //  output[(x-1)+(y-1)*OUTPUT_WIDTH]=mem[k-1][l-1];\n"\
     "}\n", defines);*/
    
    asprintf(&source_code,\
             "%s\n"\
             "kernel void filter(global uchar16 *output, global uchar *input){\n" \
             "   uchar16 left, right, dx;\n"\
             "   size_t x = get_global_id(0);\n"\
             "   size_t y = get_global_id(1);\n"\
             "   left = vload16(0, input+x*16+INPUT_WIDTH*y);\n"\
             "   right= vload16(0, input+x*16+1+INPUT_WIDTH*y);\n"\
             "   output[x+y*OUTPUT_WIDTH/16]=abs_diff(left, right);\n"\
             "}\n", defines);
    
    
    BLCL_ERROR_CHECK(cl_output=clCreateBuffer(target->context, CL_MEM_WRITE_ONLY, output->size, NULL, &error_id), NULL, error_id,  NULL);
    BLCL_ERROR_CHECK(cl_input=clCreateBuffer(target->context, CL_MEM_READ_ONLY, input->size, NULL, &error_id), NULL, error_id,  NULL);
    kernel=BLCL_CREATE_KERNEL_2P(target, "filter", source_code, cl_output, cl_input);
    
    free(defines);
    free(source_code);
    return kernel;
}

void quit()
{
    output.unlink();
    blcl_release(&gpu_target);
    blc_quit();
}

int main(int argc, char **argv)
{
    cl_kernel kernel;
    
    blc_stderr_ansi_detect();
    blc_program_add_channel_option(&output, 'o', "output", "output channel name", "/retina");
    blc_program_add_input_pipe_option(stdin, 'I', "input-pipe", "input pipe name", NULL);
    blc_program_add_channel_parameter(&input, 1, "input image");

    blc_program_option_interpret_and_print_title(&argc, &argv);
    
    input.open();
    output.create_or_open(BLC_SEM_PROTECT, 'UIN8', 'Y800', NULL, 2, input.lengths[0], input.lengths[1]);
    printf("%s\n", output.name);
    atexit(quit);
    
    blcl_init(&gpu_target, CL_DEVICE_TYPE_GPU);
    kernel=update_kernel(&gpu_target, &output, &input);
    items_nbs[0]=output.lengths[0]/16;
    items_nbs[1]=((output.lengths[1]-1)/16)*16;
    work_group_sizes[0]=1;
    work_group_sizes[1]=1;
    
    BLC_PROGRAM_FOR_LOOP()
    {
        input.protect();
        BLCL_CHECK(clEnqueueWriteBuffer(gpu_target.command_queue, cl_input, CL_TRUE, 0, input.size, input.data, 0, NULL, NULL ), "Failed to write input array!");
        input.protect(0);
        BLCL_CHECK(clEnqueueNDRangeKernel(gpu_target.command_queue, kernel, 2, NULL, items_nbs, work_group_sizes, 0, NULL, NULL), "Failed to execute kernel! items_nb '%lu', work_group_size '%lu'", items_nbs[0], work_group_sizes[0]);
        BLCL_CHECK(clFinish(gpu_target.command_queue), "finishing");
        output.protect();
        BLCL_CHECK(clEnqueueReadBuffer(gpu_target.command_queue, cl_output, CL_TRUE, 0, output.size, output.data, 0, NULL, NULL ), "Failed to read output array!");
        output.protect(0);
    }
    return 0;
}
