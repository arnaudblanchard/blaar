//
//  Created by Arnaud Blanchard on 06/11/14.
//  Copyright ETIS 2014. All rights reserved.
//

#include "blc.h"

blc_channel  *output_channel=NULL;
char data_name[NAME_MAX];
double period;

static void quit()
{
    fflush(stdout);
    if (output_channel) output_channel->unlink();
    printf("\nQuitting convert_image.");
}


static void start_processing(char const *input)
{
    blc_channel *input_channel, *output_image;
    blc_mem memory, ON, OFF, dx, dy, dX, dY;
    char ch;
    char channel_name[NAME_MAX+1];
    int ret, width, height;
    int bipolar_width, bipolar_height;
    int i,j,k;
    uint32_t format_str;
    struct timeval timer;
    
    atexit(quit);
    
    if (input==NULL)
    {
        ret = scanf( "%s", channel_name);
        if (ret != 1) EXIT_ON_ERROR("Fail reading channel name");
    }
    else STRCPY(channel_name, input);
    
    input_channel = new blc_channel(channel_name);
    memory.allocate(input_channel->size);
    
    width=input_channel->lengths[0];
    height=input_channel->lengths[1];
    
    bipolar_width=width-2;
    bipolar_height=height-2;
    ON.allocate(bipolar_width*bipolar_height);
    OFF.allocate(bipolar_width*bipolar_height);
    
    dx.allocate((width-1)*height);
    dX.allocate((width-1)*height);
    dy.allocate(width*(height-1));
    dY.allocate(width*(height-1));
    
    if (input_channel->format == STRING_TO_UINT32("Y800"))
    {
        output_channel=new blc_channel("/bipolar", BLC_SEM_PROTECT, 'UIN8', 'Y800', NULL, 2, bipolar_width, bipolar_height);
        printf("%s\n", output_channel->name);
    }
    else EXIT_ON_ERROR("Pixel format '%4.s' is not managed", UINT32_TO_STRING(format_str, input_channel->format));
    
    while(1)
    {
        ch = getc(stdin);
        switch  (ch)
        {
            case 'i':printf("i");
                fflush(stdout);
                break;
            case 'q':
                printf("q");
                fflush(stdout);
                exit(0);
                break;
        }
        
        /***
         dx  \
         Dx  /
         ON  /\ Dx[0]+dx[+1]
         OFF \/ Dx[1]+dx
         */
        
        input_channel->protect();
        us_time_diff(&timer);
        FOR(i, width-1)
        FOR(j, height)
        {
            dx.data[i+j*(width-1)]=CLIP_UCHAR((uchar)input_channel->data[i+j*width]-(uchar)input_channel->data[i+1+j*width]);
            dX.data[i+j*(width-1)]=CLIP_UCHAR((uchar)input_channel->data[i+1+j*width]-(uchar)input_channel->data[i+j*width]);
        }
        fprintf(stderr, "%.3f \n", us_time_diff(&timer)/100.f);
        
        FOR(i, width)
        FOR(j, height-1)
        {
            dy.data[i+j*width]=CLIP_UCHAR((uchar)input_channel->data[i+j*width]-(uchar)input_channel->data[i+(1+j)*width]);
            dY.data[i+j*width]=CLIP_UCHAR((uchar)input_channel->data[i+(1+j)*width]-(uchar)input_channel->data[i+j*width]);
        }
        
        /*
         ON:       |  OFF:
         0  -1  0  |  0   1  0
         -1  4 -1  |  1  -4  1
         0  -1  0  |  0   1  0
         */
        FOR(i, bipolar_width)
        FOR(j, bipolar_height)
        {
            ON.data[i+j*bipolar_width]=CLIP_UCHAR((uchar)dx.data[i+1+(j+1)*(width-1)]+(uchar)dX.data[i+(j+1)*(width-1)]+(uchar)dy.data[(i+1)+(j+1)*width]+(uchar)dY.data[i+1+j*width]);
            OFF.data[i+j*bipolar_width]=CLIP_UCHAR((uchar)dx.data[i+(j+1)*(width-1)]+(uchar)dX.data[i+1+(j+1)*(width-1)]+(uchar)dy.data[i+1+j*width]+(uchar)dY.data[i+1+(j+1)*width]);
        }
        
        /*
         FOR(i, bipolar_width-1)
         FOR(j, bipolar_height-1)
         {
         ON.data[i+j*bipolar_width]= ON.data[i+j*bipolar_width]
         OFF.data[i+j*bipolar_width]=CLIP_UCHAR((uchar)dx.data[i+(j+1)*(width-1)]+(uchar)dX.data[i+1+(j+1)*(width-1)]+(uchar)dy.data[i+1+j*width]+(uchar)dY.data[i+1+(j+1)*width]);
         }
         */
        
        input_channel->protect(0);
        output_channel->protect();
        
        FOR_INV(k, output_channel->size)
        {
            output_channel->data[k]=CLIP_UCHAR(((uchar)OFF.data[k]+(uchar)ON.data[k]));
            
        }
        output_channel->protect(0);
        printf(".");
        fflush(stdout);
    }
}

int main(int argc, char **argv)
{
    size_t k;
    int i, j;
    int width, height;
    char *data_format;
    char const *input = NULL;
    uint32_t input_pixel_format, output_pixel_format=STRING_TO_UINT32("yuv2");
    char const *neural_type="delta";
    char tmp_sem_abp[4];
    uint32_t tmp_type;
    size_t tmp_size;
    int ret, ch;
    blc_channel *image;
    const char *help;
    
    program_option_add(&input, 'i', "data", "blc_channel", "input data to convert", NULL);
    program_option_add(&help, 'h', "help", NULL, "display help", NULL);
    
    program_option_interpret(&argc, &argv);
    
    if (help) program_option_display_help();
    else start_processing(input);
    
    
    return 0;
}
