//
//  Created by Arnaud Blanchard on 06/01/16.
//  Copyright ETIS 2014. All rights reserved.
//
#include "blc.h"
#include "blcl.h"
#include <sys/signal.h>
#include <stdlib.h>
#include <unistd.h>

enum {RUN, PAUSE, STOP, STATUS_NB} state;
struct blcl_target gpu_target;
size_t work_group_sizes[2];
size_t items_nbs[2];

double period;
int iteration=0;
int input_pipe, output_pipe;

char const *input_pipe_name, *output_channel_name, *clusters_channel_name;

int refresh_filter=1;
int clusters_max;
cl_mem cl_input, cl_output, cl_clusters;
blc_channel output, input, clusters;
cl_kernel kernel;

char *activation=(char*)"convert_uchar_sat(p)";

// Simple compute kernel which computes the square of an input array
cl_kernel update_kernel(blcl_target *target, blc_narray *output, blc_narray *input, blc_narray *clusters)
{
    char  *defines, *source_code;
    cl_int error_id;
    cl_kernel kernel;
    
    asprintf(&defines,
             "#define SIZE %d\n#define CLUSTERS_MAX %d\n", input->lengths[0], clusters_max);
    
    //un test de distance par par work_group
    asprintf(&source_code,\
             "%s\n"\
             "kernel void saw(global float *selection, global uchar *input, global uchar *clusters){\n" \
             "   size_t i = get_local_id(0);\n"\
             "   size_t j = get_group_id(0);\n"\
             "   local uint d2[SIZE];\n"\
             "   local uchar mem[SIZE];\n"\
             "   mem[i]=input[i];\n"\
             "   d2[i]=abs_diff(input[i], clusters[SIZE*j+i]);\n"\
             "   d2[i]=3; //d2[i];"\
             "   for(uint offset = SIZE/2; offset != 0; offset /= 2) {\n"\
             "    barrier(CLK_LOCAL_MEM_FENCE);\n"\
             "    if (i < offset)\n"\
             "    {\n"\
             "       d2[i] += d2[i + offset];\n"\
             "       if (offset==1) selection[j]=d2[0]; //1.f/(1.f+sqrt((float)d2[0]));\n"\
             "    }\n"\
             "   }\n"\
             "}\n", defines);
    
    
    BLCL_ERROR_CHECK(cl_output=clCreateBuffer(target->context, CL_MEM_WRITE_ONLY, output->size, NULL, &error_id), NULL, error_id,  NULL);
    BLCL_ERROR_CHECK(cl_input=clCreateBuffer(target->context, CL_MEM_READ_ONLY, input->size, NULL, &error_id), NULL, error_id,  NULL);
    BLCL_ERROR_CHECK(cl_clusters=clCreateBuffer(target->context, CL_MEM_READ_WRITE, clusters->size, NULL, &error_id), NULL, error_id,  NULL);
    kernel=BLCL_CREATE_KERNEL_3P(target, "saw", source_code, cl_output, cl_input, cl_clusters);
    
    free(defines);
    free(source_code);
    return kernel;
}

static void quit()
{
    if (output_pipe) {
        
        printf("q");
        fflush(stdout);
    }
    output.unlink();
    clusters.unlink();
    blcl_release(&gpu_target);
    
    blc_quit();
}

void command_cb(char const *command, char const *, void *)
{
    switch (command[0])
    {
        case 0: if (state==RUN) {
            state=PAUSE;
            blc_command_display_help();
            blc_command_interpret();
        }else state=RUN;
            break;
        case 'q': state=STOP;
            break;
    }
}

int main(int argc, char **argv)
{
    char const *help;
    char const *output_format_option;
    char const *clusters_max_option;
    char const *vigilence_option;
    struct timeval timer;
    int i, imax, clusters_nb=0;
    float *selection, max, vigilence;
    
    blc_terminal_ansi_detect();
    
    program_option_add(&output_format_option, 'f', "format", "Y800", "set ouput video format", "NDEF");
    program_option_add(&help, 'h', "help", 0, "display help", NULL);
    program_option_add(&output_channel_name, 'o', "output", "blc_channel", "output channel name", NULL);
    program_option_add(&clusters_max_option, 's', "size", "integer", "maximum number of clusters", "10000");
    program_option_add(&input_pipe_name, 'I', "input-pipe", "pipe_name", "input pipe name", NULL);
    program_option_add(&vigilence_option, 'v', "vigilence", "float", "vigilence threshold", "0.9");

    program_option_interpret_and_print_title(&argc, &argv);
    
    if (help) program_option_display_help();
    else{
        if (input_pipe_name){
            freopen(input_pipe_name, "r", stdin);
            fprintf(stderr, "waiting pipe '%s'\n", input_pipe_name);
        }
        
        if (argc==1) {
            fprintf(stderr, "Input channel name: ");
            SYSTEM_ERROR_CHECK(fgets(input.name, sizeof(input.name), stdin), NULL, "Wrong channel input name");
            input.name[strlen(input.name)-1]=0;
        }
        else if (argc==2) STRCPY(input.name, argv[1]);
        else{
            program_option_display_help();
            EXIT_ON_ERROR("You cannot have more than two arguments.");
        }
        clusters_max= strtol(clusters_max_option, (char **)NULL, 10);
        vigilence = strtof(vigilence_option, (char **)NULL);

        
        input_pipe=!isatty(STDIN_FILENO);
        output_pipe=!isatty(STDOUT_FILENO);
        
        input.open();
        
        if (output_channel_name==NULL){
            snprintf(output.name, sizeof(clusters.name), "/selection%d", getpid());
            fprintf(stdout, "%s\n", output.name);
        }else output.set_name(output_channel_name);

        
        if (clusters_channel_name==NULL){
            snprintf(clusters.name, sizeof(clusters.name), "/clusters%d", getpid());
            fprintf(stdout, "%s\n", clusters.name);
        }
        else clusters.set_name(clusters_channel_name);
        
        output.create(BLC_SEM_PROTECT, 'FL32', STRING_TO_UINT32(output_format_option), NULL, 1, clusters_max);
        
        blcl_init(&gpu_target, CL_DEVICE_TYPE_GPU);
        blcl_fprint_device_info(stderr, gpu_target.device_id);
        
        atexit(quit);
        
        clusters.create(BLC_SEM_PROTECT, 'UIN8', 'Y800', NULL, 2, input.size, clusters_max);

        kernel=update_kernel(&gpu_target, &output, &input, &clusters);
        
        work_group_sizes[0]=input.size;
        items_nbs[0]=clusters.size;
        
        state=RUN;
        blc_command_add("", command_cb, NULL, "pause" , NULL);
        blc_command_add("q", command_cb, NULL, "quit" , NULL);

        do
        {
          //  choice=getchar();
 //           fprintf(stderr, "start\n");
 //           fflush(stderr);
                     us_time_diff(&timer);
            BLCL_CHECK(clEnqueueWriteBuffer(gpu_target.command_queue, cl_input, CL_TRUE, 0, input.size, input.data, 0, NULL, NULL ), "Failed to write input array!");
//            fprintf(stderr, "\ntime %.3fms", us_time_diff(&timer)/1000.f);

            BLCL_CHECK(clEnqueueNDRangeKernel(gpu_target.command_queue, kernel, 1, NULL, items_nbs, work_group_sizes, 0, NULL, NULL), "Failed to execute kernel! items_nb '%lu', work_group_size '%lu'", items_nbs[0], work_group_sizes[0]);
            BLCL_CHECK(clFinish(gpu_target.command_queue), "finishing");
   //         fprintf(stderr, "\ntime %.3fms", us_time_diff(&timer)/1000.f);

            output.protect();
            BLCL_CHECK(clEnqueueReadBuffer(gpu_target.command_queue, cl_output, CL_TRUE, 0, output.size, output.data, 0, NULL, NULL ), "Failed to read output array!");
            output.protect(0);
            max=0;
            imax=-1;
            selection=(float*)output.data;
        //    fprintf(stderr, "time %.3fms max:%f, imax:%d\r", us_time_diff(&timer)/1000.f,max, imax);

            FOR_INV(i, clusters_max)
            {
                if (selection[i] > max ) {
                    imax=i;
                    max=selection[i];
                }
            }
            
            if (vigilence > max && clusters_nb != clusters_max) {
                
                BLCL_CHECK(clEnqueueReadBuffer(gpu_target.command_queue, cl_clusters, CL_TRUE, 0, clusters.size, clusters.data, 0, NULL, NULL ), "Failed to read cl_clusters");
                memcpy(clusters.data+clusters_nb*input.size, input.data, input.size);
                BLCL_CHECK(clEnqueueWriteBuffer(gpu_target.command_queue, cl_clusters, CL_TRUE, 0, clusters.size, clusters.data, 0, NULL, NULL ), "Failed to write cl_clusters");
                clusters_nb++;
            }
            
            fprintf(stderr, "time %.3fms max:%f, imax:%d, clusters_nb:%d\r", us_time_diff(&timer)/1000.f, max, imax, clusters_nb);
            
            blc_command_try_to_interpret();
            
        }while(state!=STOP);
        
        return 0;
    }
}
