//
//  Created by Arnaud Blanchard on 06/01/16.
//  Copyright ETIS 2014. All rights reserved.
//
#include "blc.h"
#include "blcl.h"
#include <sys/signal.h>
#include <stdlib.h>
#include <unistd.h>

enum {RUN, PAUSE, STOP, STATUS_NB} ;
struct blcl_target gpu_target;
size_t work_group_sizes[2];
size_t items_nbs[2];

double period;
int iterations_nb=0;
int input_pipe, output_pipe;
int state;

char const *input_pipe_name, *output_channel_name, *clusters_channel_name;

int refresh_filter=1;
int clusters_max;
cl_mem cl_input, cl_output, cl_clusters;
blc_channel output, input, clusters;
cl_kernel kernel;

char *activation=(char*)"convert_uchar_sat(p)";

// Simple compute kernel which computes the square of an input array
cl_kernel update_kernel(blcl_target *target, blc_narray *output, blc_narray *input, blc_narray *clusters)
{
    char  *defines, *source_code;
    cl_int error_id;
    cl_kernel kernel;
    
    asprintf(&defines,
             "#define SIZE %d\n#define CLUSTERS_MAX %d\n", input->size/16, clusters_max);
    
    //un test de distance par par work_group
    asprintf(&source_code,\
             "%s\n"\
             "kernel void saw(global float *selection, global uchar16 *input, global uchar16 *clusters){\n" \
             "   size_t i = get_local_id(0);\n"\
             "   size_t j = get_group_id(0);\n"\
             "   uint offset;\n"\
             "   local uint16 d2[SIZE];\n"\
             "   d2[i]=convert_uint16(abs_diff(input[i], clusters[SIZE*j+i]));\n"\
             "   for(offset = SIZE/2; offset != 0; offset /= 2) {\n"\
             "      barrier(CLK_LOCAL_MEM_FENCE);\n"\
             "      if (i < offset)\n"\
             "      {\n"\
             "         d2[i].s0 += d2[i + offset].s1 + d2[i + offset].s2 + d2[i + offset].s3 + d2[i + offset].s4 + d2[i + offset].s5 + d2[i + offset].s6 + d2[i + offset].s7 + d2[i + offset].s8 + d2[i + offset].s9 + d2[i + offset].sA + d2[i + offset].sB + d2[i + offset].sC+ d2[i + offset].sD+ d2[i + offset].sE+ d2[i + offset].sF;\n"\
             "         if (offset==1) selection[j]=1.f-d2[0].s0/(144*256.f);\n"\
             "      }\n"\
             "   }\n"\
             "}\n", defines);
    
    
    BLCL_ERROR_CHECK(cl_output=clCreateBuffer(target->context, CL_MEM_WRITE_ONLY, output->size, NULL, &error_id), NULL, error_id,  NULL);
    BLCL_ERROR_CHECK(cl_input=clCreateBuffer(target->context, CL_MEM_READ_ONLY, input->size, NULL, &error_id), NULL, error_id,  NULL);
    BLCL_ERROR_CHECK(cl_clusters=clCreateBuffer(target->context, CL_MEM_READ_WRITE, clusters->size, NULL, &error_id), NULL, error_id,  NULL);
    kernel=BLCL_CREATE_KERNEL_3P(target, "saw", source_code, cl_output, cl_input, cl_clusters);
    
    free(defines);
    free(source_code);
    return kernel;
}

static void quit()
{
    if (output_pipe) {
        
        printf("q");
        fflush(stdout);
    }
    output.unlink();
    clusters.unlink();
    blcl_release(&gpu_target);
    
    blc_quit();
}

void command_cb(char const *command, char const *, void *)
{
    switch (command[0])
    {
        case 0: if (state==RUN) {
            state=PAUSE;
            blc_command_display_help();
            fprintf(stderr, "time %.3fms\n", period);
            iterations_nb=0;
            blc_command_interpret();
        }else state=RUN;
            break;
        case 'q': state=STOP;
            break;
    }
}

int main(int argc, char **argv)
{
    char const *help;
    char const *output_format_option;
    char const *clusters_max_option;
    char const *vigilence_option;
    struct timeval timer;
    int i, imax, clusters_nb=0;
    float *selection, max, vigilence;
    
    blc_stderr_ansi_detect();
    
    blc_program_option_add(&output_format_option, 'f', "format", "Y800", "set ouput video format", "NDEF");
    blc_program_option_channel_add(&output, 'o', "output", "blc_channel", "output channel name", NULL);
    blc_program_option_add(&clusters_max_option, 's', "size", "integer", "maximum number of clusters", "1800");
    program_input_add(&input_pipe_name, 'I', "input-pipe", "pipe_name", "input pipe name", NULL);
    program_option_add(&vigilence_option, 'v', "vigilence", "float", "vigilence threshold", "0.9");

    program_option_interpret_and_print_title(&argc, &argv);
    
    if (help) program_option_display_help();
    else{
        if (input_pipe_name){
            freopen(input_pipe_name, "r", stdin);
            fprintf(stderr, "waiting pipe '%s'\n", input_pipe_name);
        }
        
        if (argc==1) {
            fprintf(stderr, "Input channel name: ");
            SYSTEM_ERROR_CHECK(fgets(input.name, sizeof(input.name), stdin), NULL, "Wrong channel input name");
            input.name[strlen(input.name)-1]=0;
        }
        else if (argc==2) STRCPY(input.name, argv[1]);
        else{
            program_option_display_help();
            EXIT_ON_ERROR("You cannot have more than two arguments.");
        }
        clusters_max= strtol(clusters_max_option, (char **)NULL, 10);
        vigilence = strtof(vigilence_option, (char **)NULL);

        
        input_pipe=!isatty(STDIN_FILENO);
        output_pipe=!isatty(STDOUT_FILENO);
        
        input.open();
        
        if (output_channel_name==NULL){
            snprintf(output.name, sizeof(clusters.name), "/selection%d", getpid());
            fprintf(stdout, "%s\n", output.name);
        }else output.set_namef(output_channel_name);

        
        if (clusters_channel_name==NULL){
            snprintf(clusters.name, sizeof(clusters.name), "/clusters%d", getpid());
            fprintf(stdout, "%s\n", clusters.name);
        }
        else clusters.set_namef(clusters_channel_name);
        
        output.create(BLC_SEM_PROTECT, 'FL32', STRING_TO_UINT32(output_format_option), NULL, 1, clusters_max);
        
        blcl_init(&gpu_target, CL_DEVICE_TYPE_GPU);
        blcl_fprint_device_info(stderr, gpu_target.device_id);
        
        atexit(quit);
        
        clusters.create(BLC_SEM_PROTECT, 'UIN8', 'Y800', NULL, 2, input.size, clusters_max);

        kernel=update_kernel(&gpu_target, &output, &input, &clusters);
        
        work_group_sizes[0]=input.size/16;
        items_nbs[0]=clusters_max;
        
        state=RUN;
        blc_command_add("", command_cb, NULL, "pause" , NULL);
        blc_command_add("q", command_cb, NULL, "quit" , NULL);

        do
        {
            us_time_diff(&timer);
            input.protect();
            BLCL_CHECK(clEnqueueWriteBuffer(gpu_target.command_queue, cl_input, CL_TRUE, 0, input.size, input.data, 0, NULL, NULL ), "Failed to write input array!");
            input.protect(0);

            BLCL_CHECK(clEnqueueNDRangeKernel(gpu_target.command_queue, kernel, 1, NULL, items_nbs, work_group_sizes, 0, NULL, NULL), "Failed to execute kernel! items_nb '%lu', work_group_size '%lu'", items_nbs[0], work_group_sizes[0]);
            BLCL_CHECK(clFinish(gpu_target.command_queue), "finishing");


            output.protect();
            BLCL_CHECK(clEnqueueReadBuffer(gpu_target.command_queue, cl_output, CL_TRUE, 0, output.size, output.data, 0, NULL, NULL ), "Failed to read output array!");
            output.protect(0);
            max=0;
            imax=-1;
            selection=(float*)output.data;

            max=0;
            FOR_INV(i, clusters_max)
            {
                if (selection[i] > max ) {
                    imax=i;
                    max=selection[i];
                }
            }
            
            if (vigilence > max && clusters_nb != clusters_max) {
                
                BLCL_CHECK(clEnqueueReadBuffer(gpu_target.command_queue, cl_clusters, CL_TRUE, 0, clusters.size, clusters.data, 0, NULL, NULL ), "Failed to read cl_clusters");
                memcpy(clusters.data+clusters_nb*input.size, input.data, input.size);
                BLCL_CHECK(clEnqueueWriteBuffer(gpu_target.command_queue, cl_clusters, CL_TRUE, 0, clusters.size, clusters.data, 0, NULL, NULL ), "Failed to write cl_clusters");
                clusters_nb++;
            }
            iterations_nb++;
            period=period+(us_time_diff(&timer)/1000.f-period)/iterations_nb;
            usleep(30000);
            blc_command_try_to_interpret();
        }while(state!=STOP);
        
        return 0;
    }
}
